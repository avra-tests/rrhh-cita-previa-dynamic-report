#!/bin/bash

export OKTETO_GIT_BRANCH=`git rev-parse --abbrev-ref HEAD` 

echo "deploy in local desde imagen registry.gitlab.com/avra-tests/rrhh-cita-previa-dynamic-report/app:${OKTETO_GIT_BRANCH}"
docker rmi "registry.gitlab.com/avra-tests/rrhh-cita-previa-dynamic-report/app:${OKTETO_GIT_BRANCH}"
docker-compose up