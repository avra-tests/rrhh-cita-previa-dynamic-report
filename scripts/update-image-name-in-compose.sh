#!/bin/bash

echo "Este script debe ejecutarse desde el directorio raiz del repositorio: ./scripts/xxx.sh"

VERSION=`git rev-parse --abbrev-ref HEAD`
# .`git rev-parse HEAD | cut -c 1-7`
PROJECT="registry.gitlab.com/avra-tests/rrhh-cita-previa-dynamic-report"
CONTAINER_REGISTRY="registry.gitlab.com"
IMAGE_NAME_WITH_TAG=$CONTAINER_REGISTRY/$PROJECT:$VERSION

echo "echo IMAGE_NAME=$IMAGE_NAME_WITH_TAG > .env"
echo "IMAGE_NAME=$IMAGE_NAME_WITH_TAG" > .env