#!/bin/bash

if [ $# -ne 4 ]
then
    echo "Uso del script: review-app NETLIFY_SITE_ID NETLIFY_AUTH_TOKEN MERGE_REQUEST_IID COMMIT_REF_NAME"
    exit 127 
fi

NETLIFY_SITE_ID=$1
NETLIFY_AUTH_TOKEN=$2
MERGE_REQUEST_IID=$3
COMMIT_REF_NAME=$4

# echo "$1 $2 $3 $4"
# echo npx netlify deploy $NETLIFY_SITE_ID --auth $NETLIFY_AUTH_TOKEN --alias "review-app-$MERGE_REQUEST_IID" --message "Review app deploy desde rama $COMMIT_REF_NAME"

npx netlify deploy $NETLIFY_SITE_ID --auth $NETLIFY_AUTH_TOKEN --alias "review-app-$MERGE_REQUEST_IID" --message "Review app deploy desde rama $COMMIT_REF_NAME"