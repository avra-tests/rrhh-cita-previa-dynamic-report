#!/bin/bash

echo "Uso: deploy-preview-to-okteto $PREVIEW_NAME $BRANCH_NAME $REPOSITORY_URL"
# VERSION=`git rev-parse --abbrev-ref HEAD`.`git rev-parse HEAD | cut -c 1-7`
# BRANCH=`git rev-parse --abbrev-ref HEAD`
# PROJECT="/avra-tests/rrhh-cita-previa-dynamic-report"
# REPOSITORY_URL="https://gitlab.com$PROJECT"
# PREVIEW_NAME="review-$BRANCH-$OKTETO_USERNAME"


echo "okteto preview deploy $PREVIEW_NAME --scope personal --branch $BRANCH_NAME --repository $REPOSITORY_URL --wait"
okteto preview deploy $PREVIEW_NAME --scope personal --branch $BRANCH_NAME --repository $REPOSITORY_URL --wait


# Para pruebas
# okteto stack deploy --build
# okteto stack destroy 

# Para prod debe ser gitlab pages