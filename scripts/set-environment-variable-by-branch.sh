#!/bin/bash

# "Uso del script: ./scripts/set-environment-variable-by-branch BRANCH_NAME"

BRANCH_NAME=$1

case $BRANCH_NAME in
    "")
        VARIABLE_ENTONNO="ERROR: Rama / Entorno no definido"
        ;;
    "produccion")
        VARIABLE_ENTONNO="PRODUCCION"
        ;;
    "main")
        VARIABLE_ENTONNO="PRUEBAS"
        ;;
    *)
        VARIABLE_ENTONNO="DESARROLLO"
        ;;
esac

echo $VARIABLE_ENTONNO
exit 0



