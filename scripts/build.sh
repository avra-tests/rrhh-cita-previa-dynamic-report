#!/bin/bash

echo "Uso: ./scripts/build.sh BRANCH_NAME VARIABLE_ENTONNO_PRODUCCION VARIABLE_ENTONNO_PRUEBAS VARIABLE_ENTONNO_DESARROLLO" 

BRANCH_NAME=$1 

case $BRANCH_NAME in
    "")
        VARIABLE_ENTONNO="ERROR: Rama / Entorno no definido"
        ;;
    "produccion")
        VARIABLE_ENTONNO=$2
        ;;
    "main")
        VARIABLE_ENTONNO=$3
        ;;
    *)
        VARIABLE_ENTONNO=$4
        ;;
esac

echo "Ejecutando ENTONNO=$VARIABLE_ENTONNO npx quasar build"

ENTONNO=$VARIABLE_ENTONNO npx quasar build