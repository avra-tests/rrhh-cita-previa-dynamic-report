import fs from "fs";

const jsonFilePath = "../public/static-rest/appointments-orig.json";
const jsonObsFilePath = "../public/static-rest/appointments.json";

const appointmentsWithoutObfuscate = getDataForFromFile(jsonFilePath);

const appointmentsObfuscated = appointmentsWithoutObfuscate.appointmentList.map(
  (appointment) => {
    const customer = appointment.customers[0];
    const newAppointment = !appointment.customers[0]
      ? appointment
      : {
          ...appointment,
          notes: obfuscateString(appointment.notes),
          customers: [
            {
              ...customer,
              name: `Nombre${customer.id}`,
              firstName: `Ap1${customer.id}`,
              lastName: `Ap2${customer.id}`,
              email: `nombre.${customer.id}@correito.com`,
              phone: obfuscateString(customer.phone),
              externalId: obfuscateString(customer.externalId),
            },
          ],
        };
    
    return newAppointment;
  }
);

// console.log(appointmentsObfuscated[0].customers);

writeJSONFile(jsonObsFilePath, {
  ...appointmentsWithoutObfuscate,
  appointmentList: appointmentsObfuscated,
});

function obfuscateString(stringToObfuscate) {
  return btoa(stringToObfuscate + "a");
}

function getDataForFromFile(dataFilePath) {
  const dataFromFile = JSON.parse(fs.readFileSync(dataFilePath));

  return dataFromFile;
}

function writeJSONFile(filePath, json) {
  fs.writeFileSync(filePath, JSON.stringify(json));
}
