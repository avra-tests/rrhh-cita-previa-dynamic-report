#!/bin/bash

echo "Este script debe ejecutarse desde el directorio raiz del repositorio: ./scripts/create-and-push-image-for-deploy-app.sh"

VERSION=`git rev-parse --abbrev-ref HEAD`
# .`git rev-parse HEAD | cut -c 1-7`
PROJECT="/avra-tests/rrhh-cita-previa-dynamic-report/app"
CONTAINER_REGISTRY="registry.gitlab.com"
IMAGE_NAME_WITH_TAG=$CONTAINER_REGISTRY$PROJECT:$VERSION
# IMAGE_NAME_WITH_TAG=$CONTAINER_REGISTRY$PROJECT:$1

echo $IMAGE_NAME_WITH_TAG

docker login registry.gitlab.com &&
docker build -f ./dockerfile-for-deploy-app -t $IMAGE_NAME_WITH_TAG . &&
docker push $IMAGE_NAME_WITH_TAG

