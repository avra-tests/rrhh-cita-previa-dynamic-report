import Vue from "vue";
import VueAppShared from "src/vue-app-shared";
import { AppConfiguration } from "src/vue-app-shared";
import appConfigToApply from "src/app-config.js";

import { boot } from "quasar/wrappers";

export default boot(async ({ router, store, Vue }) => {
  let appConfig = new AppConfiguration(appConfigToApply, router, store);
  appConfig.applyConfiguration();

  Vue.use(VueAppShared);
});
