const AsMainLayout = () => import("src/vue-app-shared/layouts/AsMainLayout");

const routes = [
  {
    path: "/",
    redirect: "/informe-detalle-citas"
  },
  {
    path: "/informe-detalle-citas",
    component: AsMainLayout,
    // @ts-ignore
    children: [{ path: "", component: () => import("pages/InformeDetalleCitas.vue") }]
  },
  // {
  //   path: "/informe-citas-por-dia",
  //   component: AsMainLayout,
  //   // @ts-ignore
  //   children: [{ path: "", component: () => import("pages/InformeCitasPorDias.vue") }]
  // },
  // {
  //   path: "/empleados",
  //   component: AsMainLayout,
  //   // @ts-ignore
  //   children: [{ path: "", component: () => import("pages/Empleados.vue") }]
  // },
  // {
  //   path: "/finalidades",
  //   component: AsMainLayout,
  //   // @ts-ignore
  //   children: [{ path: "", component: () => import("pages/Finalidad.vue") }]
  // },
  // {
  //   path: "/tipoFuente",
  //   component: AsMainLayout,
  //   // @ts-ignore
  //   children: [{ path: "", component: () => import("pages/TipoFuente.vue") }]
  // },
  // {
  //   path: "/hitos",
  //   component: AsMainLayout,
  //   // @ts-ignore
  //   children: [{ path: "", component: () => import("pages/Hito.vue") }]
  // },
  // {
  //   path: "/planFinanciacion",
  //   component: AsMainLayout,
  //   // @ts-ignore
  //   children: [{ path: "", component: () => import("pages/PlanFinanciacion.vue") }]
  // },
  // {
  //   path: "/actuacion",
  //   component: AsMainLayout,
  //   // @ts-ignore
  //   children: [{ path: "", component: () => import("pages/Actuacion.vue") }]
  // }
  // {
  //   path: "/empleados/:id",
  //   component: AsMainLayout,
  //   // @ts-ignore
  //   children: [{ path: "", component: () => import("pages/EmpleadosItem.vue") }]
  // }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  // @ts-ignore
  routes.push({
    path: "*",
    // @ts-ignore
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
