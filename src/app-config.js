import {
  CRUDEntityProviderJSONFile,
  CRUDEntityProviderJSONFileConfig,
  CRUDEntityProviderQmaticCita,
  CRUDEntityProviderQmaticPerfilCita,
  CRUDEntityProviderQmaticConfig,
  CRUDEntityProviderWSO2EI,
  CRUDEntityProviderWSO2EIConfig,
  utils,
} from "src/vue-app-shared";

import Cita from "src/store/citas/Cita.js";
import PerfilCita from "src/store/citas/PerfilCita.js";
import Informe from "src/store/citas/Informe";

const sameAppServerUrl = `${utils.getCurrentBaseUrlWithoutRoute(window.location)}`;
console.log("🚀 ~ file: app-config.js ~ line 17 ~ sameAppServerUrl", sameAppServerUrl)
const publicFolderWithJsonFiles = "/static-rest";



const qmaticServerUrl = `//${window.location.host}`; // "http://sgce.epsa.junta-andalucia.es:8080";
const qmaticApiBasePath = "/api-qmatic"; // /api-qmatic/calendar-backend/api/v1

const nowDate = new Date();
const startDate = utils.dateWithDaysAdded(nowDate, 0);
const endDate = utils.dateWithDaysAdded(nowDate, 60);
// const startDate = new Date(Date.parse("17 Mar 2022"));;
// const endDate = new Date(Date.parse("18 Mar 2022"));;

let appConfig = {
  startDate: startDate,
  endDate: endDate,
  menuItems: [
    // {
    //   label: "Huecos libres",
    //   icon: "pivot_table_chart",
    //   routePath: "/informe-huecos",
    // },
    // {
    //   label: "Citas por oficina y fecha ",
    //   icon: "event_note",
    //   routePath: "/informe-citas-oficinas",
    // },
    {
      label: "Detalle de citas",
      icon: "table_chart",
      routePath: "/informe-citas-hoy",
    },
    // {
    //   label: "Citas por día",
    //   icon: "event",
    //   routePath: "/informe-citas-por-dia",
    // },
    // { label: "Empleados", icon: "group", routePath: "/empleados" },
    // { label: "Finalidades", icon: "assignment_ind", routePath: "/finalidades" },
    // { label: "Tipos de Fuente de Financiación", icon: "monetization_on", routePath: "/tipoFuente" },
    // { label: "Definición de Hitos", icon: "event", routePath: "/hitos" },
    // { label: "Plan de Financiación", icon: "assessment", routePath: "/planFinanciacion" },
    // { label: "Busqueda de Actuaciones", icon: "handyman", routePath: "/actuacion" }
  ],
  entityConfigs: [
    // {
    //   entityClassNameToProvide: "Cita",
    //   entityClassToProvide: Cita,
    //   providerConfigClass: CRUDEntityProviderQmaticConfig,
    //   providerClass: CRUDEntityProviderQmaticCita,
    //   restServerUrl: qmaticServerUrl, //qmaticServerUrl, // Para evitar el CORSSSS
    //   apiBasePath: qmaticApiBasePath,
    //   // resourceName: `/appointments?start=${utils.getYYMMDDDateString(nowDate)}&end=${utils.getYYMMDDDateString(
    //   //   endDate
    //   // )}&timeZoneBranchId=9&resource=30%2C41`,
    //   resourceName: `/appointments?start=${utils.getYYMMDDDateString(
    //     startDate
    //   )}&end=${utils.getYYMMDDDateString(endDate)}`,
    //   useLocalCachedDB: true,
    // },
    // {
    //   entityClassNameToProvide: "PerfilCita",
    //   entityClassToProvide: PerfilCita,
    //   providerConfigClass: CRUDEntityProviderQmaticConfig,
    //   providerClass: CRUDEntityProviderQmaticPerfilCita,
    //   restServerUrl: qmaticServerUrl, // Para evitar el CORSSSS
    //   apiBasePath: qmaticApiBasePath,
    //   // resourceName: "/appointmentprofiles?branch=2&resourceGroup=2",
    //   resourceName: "/appointmentprofiles",
    //   useLocalCachedDB: true,
    // },
    {
      entityClassNameToProvide: "Cita",
      entityClassToProvide: Cita,
      providerConfigClass: CRUDEntityProviderQmaticConfig,
      providerClass: CRUDEntityProviderQmaticCita,
      restServerUrl: sameAppServerUrl, //qmaticServerUrl, // Para evitar el CORSSSS
      apiBasePath: publicFolderWithJsonFiles,
      // resourceName: `/appointments?start=${utils.getYYMMDDDateString(nowDate)}&end=${utils.getYYMMDDDateString(
      //   endDate
      // )}&timeZoneBranchId=9&resource=30%2C41`,
      resourceName: `/appointments.json`,
      useLocalCachedDB: true,
    },
    {
      entityClassNameToProvide: "PerfilCita",
      entityClassToProvide: PerfilCita,
      providerConfigClass: CRUDEntityProviderQmaticConfig,
      providerClass: CRUDEntityProviderQmaticPerfilCita,
      restServerUrl: sameAppServerUrl, // Para evitar el CORSSSS
      apiBasePath: publicFolderWithJsonFiles,
      // resourceName: "/appointmentprofiles?branch=2&resourceGroup=2",
      resourceName: "/appointmentprofiles.json",
      useLocalCachedDB: true,
    },     
    {
      entityClassNameToProvide: "Informe",
      entityClassToProvide: Informe,
      providerConfigClass: CRUDEntityProviderJSONFileConfig,
      providerClass: CRUDEntityProviderJSONFile,
      restServerUrl: sameAppServerUrl,
      apiBasePath: publicFolderWithJsonFiles,
      resourceName: "informes.json",
      useLocalCachedDB: true
    },
    // {
    //   entityClassNameToProvide: "Provincia",
    //   entityClassToProvide: Provincia,
    //   providerConfigClass: CRUDEntityProviderJSONFileConfig,
    //   providerClass: CRUDEntityProviderJSONFile,
    //   restServerUrl: sameAppServerUrl,
    //   apiBasePath: publicFolderWithJsonFiles,
    //   resourceName: "provincias.json",
    //   useLocalCachedDB: true
    // },
    // {
    //   entityClassNameToProvide: "Puesto",
    //   entityClassToProvide: Puesto,
    //   providerConfigClass: CRUDEntityProviderJSONFileConfig,
    //   providerClass: CRUDEntityProviderJSONFile,
    //   restServerUrl: sameAppServerUrl,
    //   apiBasePath: publicFolderWithJsonFiles,
    //   resourceName: "puestos.json",
    //   useLocalCachedDB: true
    // },
    // {
    //   entityClassNameToProvide: "Centro",
    //   entityClassToProvide: Centro,
    //   providerConfigClass: CRUDEntityProviderJSONFileConfig,
    //   providerClass: CRUDEntityProviderJSONFile,
    //   restServerUrl: sameAppServerUrl,
    //   apiBasePath: publicFolderWithJsonFiles,
    //   resourceName: "centros-trabajo.json",
    //   useLocalCachedDB: true
    // },
    // {
    //   entityClassNameToProvide: "Favorito",
    //   entityClassToProvide: Favorito,
    //   providerConfigClass: CRUDEntityProviderJSONFileConfig,
    //   providerClass: CRUDEntityProviderJSONFile,
    //   restServerUrl: sameAppServerUrl,
    //   apiBasePath: publicFolderWithJsonFiles,
    //   resourceName: "favoritos.json",
    //   useLocalCachedDB: true
    // }
    // {
    //   entityClassToProvide: Finalidad,
    //   providerConfigClass: CRUDEntityProviderWSO2EIConfig,
    //   providerClass: CRUDEntityProviderWSO2EI,
    //   restServerUrl: esbURL,
    //   apiBasePath: esbApiName,
    //   resourceName: "finalidad",
    //   useLocalCachedDB: false
    // },
    // {
    //   entityClassToProvide: TipoFuente,
    //   providerConfigClass: CRUDEntityProviderWSO2EIConfig,
    //   providerClass: CRUDEntityProviderWSO2EI,
    //   restServerUrl: esbURL,
    //   apiBasePath: esbApiName,
    //   resourceName: "tipo-fuente",
    //   useLocalCachedDB: false
    // },
    // {
    //   entityClassToProvide: Hito,
    //   providerConfigClass: CRUDEntityProviderWSO2EIConfig,
    //   providerClass: CRUDEntityProviderWSO2EI,
    //   restServerUrl: esbURL,
    //   apiBasePath: esbApiName,
    //   resourceName: "hito",
    //   useLocalCachedDB: false
    // },
    // {
    //   entityClassToProvide: Dominio,
    //   providerConfigClass: CRUDEntityProviderWSO2EIConfig,
    //   providerClass: CRUDEntityProviderWSO2EI,
    //   restServerUrl: esbURL,
    //   apiBasePath: esbApiName,
    //   resourceName: "dominio",
    //   useLocalCachedDB: false
    // },
    // {
    //   entityClassToProvide: PlanFinancion,
    //   providerConfigClass: CRUDEntityProviderWSO2EIConfig,
    //   providerClass: CRUDEntityProviderWSO2EI,
    //   restServerUrl: esbURL,
    //   apiBasePath: esbApiName,
    //   resourceName: "plan-financiacion",
    //   useLocalCachedDB: false
    // },
    // {
    //   entityClassToProvide: Actuacion,
    //   providerConfigClass: CRUDEntityProviderWSO2EIConfig,
    //   providerClass: CRUDEntityProviderWSO2EI,
    //   restServerUrl: esbURL,
    //   apiBasePath: esbApiName,
    //   resourceName: "actuacion",
    //   useLocalCachedDB: false
    // }
  ],
};

console.log("🚀 ~ file: app-config.js ~ line 197 ~  appConfig",  appConfig)

export default appConfig;

