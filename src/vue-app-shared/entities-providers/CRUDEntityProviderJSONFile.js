import AppError from "../shared/AppError";
import { EntityList, EntityListParams } from "../shared/EntityList";
import { CRUDEntity, CRUDEntityProvider } from "../shared/CRUDEntity";

import utils from "../shared/utils";

import axios from "axios";
import { AxiosInstance, AxiosPromise, AxiosError, AxiosResponse, AxiosRequestConfig } from "axios";

class CRUDEntityProviderJSONFileError extends AppError {
  constructor(msg, currentResourceConfig) {
    let newMsg = `RestResourceJSONFileProvider: ${msg}. Config: ${currentResourceConfig}`;
    super(newMsg);
  }
}

class CRUDEntityProviderJSONFileConfig {
  /** @type  {string} */
  restServerUrl;

  /** @type  {string} */
  apiBasePath;

  /** @type  {string} */
  resourceName;

  /** @type {FunctionConstructor} */
  entityClassToProvide;

  /** @type  {Function} */
  responseToEntityFieldsValuesFunction;

  /** @type  {Function} */
  responseToEntityListFieldsValuesFunction;

  constructor(fieldsValues) {
    utils.copyValuesInClassObjectProperties(fieldsValues, this);
  }
}

/**
 *
 * @param {*} response
 * @param {*} entityClassToProvide
 * @param {EntityListParams} listParams
 */
const rawJsonArrayResponseToEntityListValuesFunction = function(response, entityClassToProvide, listParams) {
  let entityListItems = [];

  try {
    let responseListItems = [];
    // Esto es porque el response viene con p.e { "Empleados": "[]"}
    responseListItems = utils.getFirstFieldValueOfObject(response.data);

    // esto es pq lo servicios entregados no lo devuelven nada por ahora
    if (!response.headers["X-Total-Count"]) {
      // TODO: cambiar cuando esté el servicio guay
      response.headers["X-Total-Count"] = responseListItems.length;
    }

    entityListItems = responseListItems.map(item => {
      let entityListNewItem = utils.entityObjectFromFieldsValuesObject(item, entityClassToProvide);
      return entityListNewItem;
    });
  } catch (error) {
    throw new AppError(`Error conviertiendo response a entidad ${response}, excepcion: ${error}`);
  }

  /** @type {EntityList} */
  let list = new EntityList({
    items: entityListItems,
    itemsTotalCount: response.headers["X-Total-Count"],
    params: listParams
  });

  return list;
};

/**
 * Clase para obtener una recurso rest de un fichero json
 * Uso:
 * ...
 *
 */
class CRUDEntityProviderJSONFile extends CRUDEntityProvider {
  /**
   * @private
   * @type {CRUDEntityProviderJSONFileConfig}
   */
  _config;

  /**
   * Instancia del cliente http para realizar peticiones http,
   * que en este caso concreto es axios
   * @private
   * @type AxiosInstance
   */
  _httpClient;

  /**
   * @private
   * @returns AxiosInstance
   */
  _createHttpClientwithCurrentConfig() {
    let baseUrl4requests = `${this._config.restServerUrl}${this._config.apiBasePath}`;

    return axios.create({
      baseURL: baseUrl4requests
    });
  }

  /**
   * @private
   * @returns {Promise<Object>}
   */
  async _getAuthorizationHeaderForHttpRequest() {
    let addtionalHeaders = {};

    // if (this._config.authenticationUserProvider) {
    //   let authenticationTokens = await this._config.authenticationUserProvider.getAuthenticationTokens();

    //   addtionalHeaders = {
    //     "Content-Type": "application/json",
    //     token: authenticationTokens.accessToken
    //   };
    // }

    return addtionalHeaders;
  }

  /**
   * @returns {Promise<Object>} parametros que hay que añadir a la peticion http
   */
  async _getAuthorizationParamsForHttpRequest() {
    let params = {};

    // if (this._config.authenticationUserPermissionsProvider) {
    //   let permissions = await this._config.authenticationUserPermissionsProvider.readItem();
    //   params.idFuncion = permissions.usuario.idFuncion;
    // }

    return params;
  }

  /**
   * @private
   * @param {string} method
   * @param {object=} additionalRequestParams Opcional, parametros adicionales del request, como p.e. los de filtro...
   * @returns {Promise<Object>}
   */
  async _doHttpClientCommonRequest(method, additionalRequestParams) {
    // Hay que pedir los headers de autorizacion en cada peticion porque el token de acceso se va refrescando
    let additionalConfigForAutorizationHeaders = await this._getAuthorizationHeaderForHttpRequest();
    let additionalParamsForUserPermissions = await this._getAuthorizationParamsForHttpRequest();

    let requestConfig = {
      url: this._config.resourceName,
      method: method,
      headers: { ...additionalConfigForAutorizationHeaders },
      params: {
        ...additionalParamsForUserPermissions,
        ...additionalRequestParams
      }
    };

    return this._httpClient.request(requestConfig);
  }

  /**
   * Transforma un response a un objeto de la entidad que corresponda aplicando la funcion pasada
   * @param {AxiosResponse} response
   * @param {Function} responseToEntityFunction
   * @param {object=} extraParams Parametros extra, como p.e. params de un listado
   */
  _responseToEntityObject(response, responseToEntityFunction, extraParams) {
    let EntityClassToProvide = this._config.entityClassToProvide;
    let entityObject = responseToEntityFunction(response, EntityClassToProvide, extraParams);

    return entityObject;
  }

  /** PUBLIC's  *************************/

  /**
   * Crea un AuthenticationProvider inicializandolo con la configuración pasada
   * @param {CRUDEntityProviderJSONFileConfig} newConfig
   */
  constructor(newConfig) {
    super();
    this._config = newConfig;
    this._config.responseToEntityListFieldsValuesFunction = rawJsonArrayResponseToEntityListValuesFunction;
    this._httpClient = this._createHttpClientwithCurrentConfig();
  }

  /**
   * Devuelve el elementos del recurso (o entidad), cuando no se necesita buscar por id, pq solo hay un elemento de ese recurso,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   *
   * @returns {Promise<Object>}
   */
  async readItem() {
    let response = await this._doHttpClientCommonRequest("get");

    return this._responseToEntityObject(response, this._config.responseToEntityFieldsValuesFunction);
  }

  /**
   * Devuelve un un objeto EntityList con el listado (e información de filtro, paginacion...) de elementos del recurso (o entidad)
   * que cumplen ciertos criterios
   * @param {EntityListParams=} listRequestParams parametros de peticion del listado, si no se indica se pide todo el listado
   * @returns {Promise<EntityList>}
   */
  async readList(listRequestParams) {
    let response = await this._doHttpClientCommonRequest("get", listRequestParams);
    return this._responseToEntityObject(response, this._config.responseToEntityListFieldsValuesFunction, listRequestParams);
  }
}

export { CRUDEntityProviderJSONFile, CRUDEntityProviderJSONFileError, CRUDEntityProviderJSONFileConfig };
export default CRUDEntityProviderJSONFile;
