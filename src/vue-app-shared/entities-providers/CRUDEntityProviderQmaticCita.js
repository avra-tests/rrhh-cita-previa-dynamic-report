import {
  CRUDEntityProviderQmatic,
  CRUDEntityProviderQmaticConfig,
} from "./CRUDEntityProviderQmatic";
import { EntityList, EntityListParams } from "../shared/EntityList";
import utils from "../shared/utils";

import Cita from "src/store/citas/Cita";
import {
  PerfilCita,
  PlanificacionSemanal,
  PlanificacionDia,
} from "src/store/citas/PerfilCita";

/**
 * Rest service de QMatic devuelve los datos de manera que no nos interesa, aquí los adaptamos a algo más simple
 */
export default class CRUDEntityProviderQmaticCita extends CRUDEntityProviderQmatic {
  /** PUBLIC  ****/
  /**
   * @param {CRUDEntityProviderQmaticConfig} newConfig
   */
  constructor(newConfig) {
    // newConfig.responseToEntityListFieldsValuesFunction = _qmaticResponseToCitaEntityListFieldsValuesFunction;
    //newConfig.responseToEntityListFieldsValuesFunction = this.prototype._qmaticResponseToCitaEntityListFieldsValuesFunction;
    super(newConfig);
  }

  /**
   * Devuelve un un objeto EntityList con el listado (e información de filtro, paginacion...) de elementos del recurso (o entidad)
   * que cumplen ciertos criterios
   * @param {EntityListParams=} listRequestParams parametros de peticion del listado, si no se indica se pide todo el listado
   * @returns {Promise<EntityList>}
   */
  async readList(listRequestParams) {
    console.log("🚀 ~ file: CRUDEntityProviderQmaticCita.js ~ line 36 ~ CRUDEntityProviderQmaticCita ~ readList ~ listRequestParams", listRequestParams)
    let response = await this._doHttpClientCommonRequest(
      "get",
      listRequestParams
    );

    let entitiesList = this._responseToEntityListFunction(
      response,
      this._config.responseToEntityListFieldsValuesFunction,
      listRequestParams
    );
    

    let perfilesCitas = await PerfilCita.provider.readList();

    const nowDate = new Date();
    const startDate = utils.dateWithDaysAdded(nowDate, 0);
    // const startDate = new Date(Date.parse("01 Aug 2021"));
    // const endDate = utils.dateWithDaysAdded(nowDate, 30);

    // comento porque no necesita de momento los huecos y hay que pedirlos a qmatic
    // entitiesList.items = this._addAndSortHuecosSinClientes(
    //   entitiesList.items,
    //   perfilesCitas.items,
    //   startDate
    // );
    entitiesList.itemsTotalCount = entitiesList.items.length;

    return entitiesList;
  }

  /** PRIVATE *****/
  /**
   * @private
   *
   * @param {*} response
   * @param {*} entityClassToProvide
   * @param {EntityListParams} listParams
   *
   * @returns {EntityList}
   */
  _responseToEntityListFunction(response, entityClassToProvide, listParams) {
    let entityListItems = [];
    let responseListItems = [];

    responseListItems = response.data.appointmentList;
    // esto es pq lo servicios entregados no lo devuelven nada por ahora
    if (!response.headers["X-Total-Count"]) {
      // TODO: cambiar cuando esté el servicio guay
      response.headers["X-Total-Count"] = responseListItems.length;
    }

    entityListItems = responseListItems
      .map((dataItem) => this._citaEntityObjectFromDataItem(dataItem))
      .filter((cita) => this._isCitaConOficinaYServicioFianzas(cita));

    /** @type {EntityList} */
    let list = new EntityList({
      items: entityListItems,
      itemsTotalCount: response.headers["X-Total-Count"],
      params: listParams,
    });

    return list;
  }

  /**
   * 
   * @param {string} nombreServicio 
   */
  _formatNombreServicio(nombreServicio) {
    return (nombreServicio ?? "").replace(/[1-9]Fianzas/i, "Fianzas");
  }

  /**
   *
   * @param {object} dataItem
   */
  _citaEntityObjectFromDataItem(dataItem) {
    /** @typedef {object} */
    let entityListNewItem = {};

    entityListNewItem = {
      id: dataItem.id,
      titulo: dataItem.title ?? null,
      fecha: dataItem.start ?? null,
      fechaFin: dataItem.end ?? null,
      nombre: dataItem.nombre ?? null,
      apellidos: dataItem.apellidos ?? null,
      email: dataItem.email ?? null,
      telefono: dataItem.telefono ?? null,
      nif: dataItem.nif ?? null,
      oficina: dataItem.oficina ?? null,
      servicio: dataItem.servicio ?? null,
      recurso: dataItem.recurso ?? null,
      oficinaId: dataItem.oficinaId ?? null,
      servicioId: dataItem.servicioId ?? null,
      recursoId: dataItem.recursoId ?? null,
      numeroCita: dataItem.numeroCita ?? null,
      duracion: dataItem.duracion ?? null,
      tipo: dataItem.tipo ?? null,
      hora: dataItem.hora ?? null,
      horario: dataItem.horario ?? null,
      observaciones: dataItem.notes ?? dataItem.observaciones ?? "",
    };

    const isDataItemFromRestService = !!(
      dataItem.customers && dataItem.customers[0]
    );

    if (isDataItemFromRestService) {
      // Quitamos gmt porque viene mal de qmatic (mete greenwich + 0000)
      entityListNewItem.fecha = dataItem.start
        ? new Date(dataItem.start)
        : null;

      entityListNewItem.fechaFin = dataItem.start
        ? new Date(dataItem.end)
        : null;
      entityListNewItem.nombre = dataItem.customers[0].firstName ?? null;
      entityListNewItem.apellidos = dataItem.customers[0].lastName ?? null;
      entityListNewItem.email = dataItem.customers[0].email ?? null;
      entityListNewItem.telefono = dataItem.customers[0].phone ?? null;
      entityListNewItem.nif = dataItem.customers[0].externalId
        ? dataItem.customers[0].externalId !== "11111111H"
          ? dataItem.customers[0].externalId
          : null
        : null;
      entityListNewItem.oficina = dataItem.branch.name ?? null;
      entityListNewItem.oficinaId = dataItem.branch.id ?? null;
      entityListNewItem.servicio =
        dataItem.services && dataItem.services[0]
          ? dataItem.services[0].name
          : null;
      entityListNewItem.servicioId =
        dataItem.services && dataItem.services[0]
          ? dataItem.services[0].id
          : null;
      entityListNewItem.recurso = dataItem.resource.name ?? null;
      entityListNewItem.recursoId = dataItem.resource.id ?? null;
      entityListNewItem.duracion =
        dataItem.services && dataItem.services[0]
          ? dataItem.services[0].duration
          : null;
      entityListNewItem.tipo = entityListNewItem.duracion
        ? "Reserva"
        : "Hor. cerrado";
      entityListNewItem.numeroCitas = entityListNewItem.duracion ? 1 : 0;
      entityListNewItem.hora = null;
      entityListNewItem.horario = null;
      entityListNewItem.observaciones =
        dataItem.notes ?? dataItem.observaciones ?? "";
    }

    entityListNewItem.servicio = this._formatNombreServicio(entityListNewItem.servicio);

    return entityListNewItem;
  }

  /**
   *
   * @param {Cita} cita
   */
  _isCitaConOficinaYServicioFianzas(cita) {
    return (
      cita.oficina &&
      cita.oficina !== "Prueba" &&
      cita.oficina !== "Pruebas" &&
      cita.oficina !== "Parque del Alamillo"
      // &&
      // (cita.servicio === null ||
      //   cita.servicio === "" ||
      //   cita.servicio.includes("Fianzas"))
    );
  }

  /**
   *
   * @param {PerfilCita} perfil
   */
  _isPerfilCitaConOficinaYServicioFianzas(perfil) {
    return (
      perfil.oficina &&
      perfil.oficina.name !== "Prueba" &&
      perfil.oficina.name !== "Pruebas" &&
      perfil.oficina.name !== "Parque del Alamillo"
      // &&
      // perfil.servicios.some((servicio) => servicio.name.includes("Fianzas"))
    );
  }

  /**
   * @param {Array<Cita>} citas
   * @param {Array<PerfilCita>} perfilesCitas
   *
   * @return {Array<Cita>}
   */
  _addAndSortHuecosSinClientes(citas, perfilesCitas, startDate) {
    // console.log("citas:", citas);
    let citasHuecosList = [];

    let oficinasRecursosPerfil = this._oficinasRecursosPerfil(
      perfilesCitas.filter((perfil) =>
        this._isPerfilCitaConOficinaYServicioFianzas(perfil)
      )
    );

    for (const [currentOficinaId, currentOficina] of Object.entries(
      oficinasRecursosPerfil
    )) {
      for (const [currentRecursoId, currentRecurso] of Object.entries(
        currentOficina.recursos
      )) {
        currentRecurso.perfilCita.updateCitasWithHorarioLabel(
          startDate,
          currentOficina,
          currentRecurso,
          citas
        );
        citasHuecosList = citasHuecosList.concat(
          currentRecurso.perfilCita.createHuecosListForOficinaRecurso(
            startDate,
            currentOficina,
            currentRecurso,
            citas
          )
        );
      }
    }

    return this._sortCitasListByOficinaRecursosFecha(
      citasHuecosList.concat(citas)
    );
  }

  _oficinasRecursosPerfil(perfilesCitas) {
    let oficinasRecursosPerfil = {};

    perfilesCitas.forEach((currentPerfil) => {
      let currentOficina = currentPerfil.oficina;

      if (!oficinasRecursosPerfil[currentOficina.id]) {
        oficinasRecursosPerfil[currentOficina.id] = {
          id: currentOficina.id,
          name: currentOficina.name,
          recursos: {},
        };
      }

      currentPerfil.recursos.forEach((currentRecurso) => {
        let newRecurso = {
          id: currentRecurso.id,
          name: currentRecurso.name,
          perfilCita: currentPerfil,
        };
        oficinasRecursosPerfil[currentOficina.id].recursos[currentRecurso.id] =
          newRecurso;
      });
    });

    return oficinasRecursosPerfil;
  }

  // _isCitaPlanificacion(cita, oficinaId, recursoId) {
  //   return cita.oficinaId === oficinaId && cita.recursoId === recursoId;
  // }

  /**
   *
   * @param {Array<Cita>} entityListItemsCitas
   */
  _sortCitasListByOficinaRecursosFecha(entityListItemsCitas) {
    let listItemsSorted = entityListItemsCitas.sort((cita1, cita2) => {
      let compareOficina = utils.compareStrings(cita1.oficina, cita2.oficina);
      let compareRecursos = utils.compareStrings(cita1.recurso, cita2.recurso);
      let compareFechasHoras = cita1.fecha - cita2.fecha;

      if (compareOficina !== 0) {
        return compareOficina;
      } else if (compareRecursos !== 0) {
        return compareRecursos;
      } else {
        return compareFechasHoras;
      }
    });

    return listItemsSorted;
  }
}

function isDateTimeStringsSameDay(dateString1, dateString2) {
  return (
    new Date(dateString1).toDateString() ===
    new Date(dateString2).toDateString()
  );
}

/**
 *
 * @param {Date} date
 *
 * @returns {string} Devuelve algo así '2021-02-10T23:00:00.000+0100'
 */
function toISODateTimeStringWithGmtFromDate(date) {
  return new Date(date.getTime() - date.getTimezoneOffset() * 60000)
    .toISOString()
    .replace("Z", "+0000");
}

/**
 *
 * @param {Cita} cita
 */
function getFirstHourDateForCita(cita) {
  return new Date(new Date(cita.fecha).setHours(8, 0, 0, 0));
}

/**
 *
 * @param {Cita} cita
 */
function getLastHourDateTimeStringForCita(cita) {
  return new Date(new Date(cita.fecha).setHours(18, 0, 0, 0));
}
