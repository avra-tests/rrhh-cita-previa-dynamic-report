import AppError from "../shared/AppError";
import { EntityList, EntityListParams } from "../shared/EntityList";
import { CRUDEntity, CRUDEntityProvider } from "../shared/CRUDEntity";
import utils from "../shared/utils";

import axios from "axios";
import { AxiosInstance, AxiosPromise, AxiosError, AxiosResponse, AxiosRequestConfig } from "axios";

import Cita from "src/store/citas/Cita";
import { throws } from "assert";

class CRUDEntityProviderQmaticError extends AppError {
  constructor(msg, currentResourceConfig) {
    let newMsg = `RestResourceQmaticProvider: ${msg}. Config: ${JSON.stringify(currentResourceConfig)}`;
    super(newMsg);
  }
}

class CRUDEntityProviderQmaticConfig {
  /** @type  {string} */
  restServerUrl;

  /** @type  {string} */
  apiBasePath;

  /** @type  {string} */
  resourceName;

  /** @type {FunctionConstructor} */
  entityClassToProvide;

  /** @type  {Function} */
  responseToEntityFieldsValuesFunction;

  /** @type  {Function} */
  responseToEntityListFieldsValuesFunction;

  constructor(fieldsValues) {
    utils.copyValuesInClassObjectProperties(fieldsValues, this);
  }
}

/**
 * Clase para obtener una recurso rest de un fichero json
 * Uso:
 * ...
 *
 */
class CRUDEntityProviderQmatic extends CRUDEntityProvider {
  /**
   * @protected
   * @type {CRUDEntityProviderQmaticConfig}
   */
  _config;

  /**
   * Instancia del cliente http para realizar peticiones http,
   * que en este caso concreto es axios
   * @private
   * @type AxiosInstance
   */
  _httpClient;

  /**
   * @private
   * @returns AxiosInstance
   */
  _createHttpClientwithCurrentConfig() {
    let baseUrl4requests = `${this._config.restServerUrl}${this._config.apiBasePath}`;

    // console.log(
    // "🚀 ~ file: CRUDEntityProviderQmatic.js ~ line 70 ~ CRUDEntityProviderQmatic ~ _createHttpClientwithCurrentConfig ~ baseUrl4requests",
    //   baseUrl4requests
    // );

    return axios.create({
      baseURL: baseUrl4requests
    });
  }

  /**
   * @private
   * @returns {Promise<Object>}
   */
  async _getAuthorizationHeaderForHttpRequest() {
    let additionalHeaders = {};

    // TODO: Hacer esto con un authentication basic provider
    const token = `integracion:Integracion15`;
    const encodedToken = Buffer.from(token).toString("base64");

    additionalHeaders = { Authorization: "Basic " + encodedToken };

    return additionalHeaders;
  }

  /**
   * @returns {Promise<Object>} parametros que hay que añadir a la peticion http
   */
  async _getAuthorizationParamsForHttpRequest() {
    let params = {};

    // if (this._config.authenticationUserPermissionsProvider) {
    //   let permissions = await this._config.authenticationUserPermissionsProvider.readItem();
    //   params.idFuncion = permissions.usuario.idFuncion;
    // }

    return params;
  }

  /**
   * @protected
   * @param {string} method
   * @param {object=} additionalRequestParams Opcional, parametros adicionales del request, como p.e. los de filtro...
   * @returns {Promise<Object>}
   */
  async _doHttpClientCommonRequest(method, additionalRequestParams) {
    // Hay que pedir los headers de autorizacion en cada peticion porque el token de acceso se va refrescando
    let additionalConfigForAutorizationHeaders = await this._getAuthorizationHeaderForHttpRequest();
    let additionalParamsForUserPermissions = await this._getAuthorizationParamsForHttpRequest();

    let requestConfig = {
      url: this._config.resourceName,
      method: method,
      headers: { ...additionalConfigForAutorizationHeaders },
      params: {
        ...additionalParamsForUserPermissions,
        ...additionalRequestParams
      }
    };

    return this._httpClient.request(requestConfig);
  }

  /**
   *
   * Encargada de transformar response a Entity o EntityList
   *
   * @protected
   *
   * @param {*} response
   * @param {*} entityClassToProvide
   * @param {EntityListParams} listParams
   *
   * @returns {EntityList}
   */
  _responseToEntityFunction(response, EntityClassToProvide, extraParams) {
    throw new CRUDEntityProviderQmaticError(
      "Debe definirse _responseToEntityFunction para cada Entidad, pq como viene de qmatic no lo queremos",
      this._config
    );
  }

  /**
   * Transforma un response a un objeto de la entidad que corresponda aplicando la funcion pasada
   * @param {AxiosResponse} response
   * @param {Function} responseToEntityFunction
   * @param {object=} extraParams Parametros extra, como p.e. params de un listado
   */
  _responseToEntityObject(response, extraParams) {
    let EntityClassToProvide = this._config.entityClassToProvide;
    let entityObject = this._responseToEntityFunction(response, EntityClassToProvide, extraParams);

    return entityObject;
  }

  /** PUBLIC's  *************************/

  /**
   * Crea un AuthenticationProvider inicializandolo con la configuración pasada
   * @param {CRUDEntityProviderQmaticConfig} newConfig
   */
  constructor(newConfig) {
    console.log("🚀 ~ file: CRUDEntityProviderQmatic.js ~ line 169 ~ CRUDEntityProviderQmatic ~ constructor ~ newConfig", newConfig);
    super();
    if (!newConfig.responseToEntityFieldsValuesFunction || !newConfig.responseToEntityListFieldsValuesFunction) {
    }
    this._config = newConfig;
    this._config.responseToEntityListFieldsValuesFunction = () => {};
    this._httpClient = this._createHttpClientwithCurrentConfig();
  }

  /**
   * Devuelve el elementos del recurso (o entidad), cuando no se necesita buscar por id, pq solo hay un elemento de ese recurso,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   *
   * @returns {Promise<Object>}
   */
  async readItem() {
    let response = await this._doHttpClientCommonRequest("get");

    return this._responseToEntityObject(response, this._config.responseToEntityFieldsValuesFunction);
  }

  /**
   * Devuelve un un objeto EntityList con el listado (e información de filtro, paginacion...) de elementos del recurso (o entidad)
   * que cumplen ciertos criterios
   * @param {EntityListParams=} listRequestParams parametros de peticion del listado, si no se indica se pide todo el listado
   * @returns {Promise<EntityList>}
   */
  async readList(listRequestParams) {
    // console.log("readList");

    let response = await this._doHttpClientCommonRequest("get", listRequestParams);
    return this._responseToEntityObject(response, this._config.responseToEntityListFieldsValuesFunction, listRequestParams);
  }
}

export { CRUDEntityProviderQmatic, CRUDEntityProviderQmaticError, CRUDEntityProviderQmaticConfig };
export default CRUDEntityProviderQmatic;
