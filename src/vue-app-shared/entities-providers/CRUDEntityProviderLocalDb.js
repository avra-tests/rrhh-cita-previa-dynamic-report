import AppError from "../shared/AppError";
import { EntityList, EntityListParams } from "../shared/EntityList";
import { FilterField } from "../shared/Filter";
import { CRUDEntityProvider } from "../shared/CRUDEntity";

import { utils } from "../index.js";
import loki from "lokijs";

class CRUDEntityProviderLocalDbConfig {
  // Se tiene que guardar pq en la minimificación se pierde el nombre original de la clase
  /** @type {String} */
  entityClassNameToProvide;

  /** @type {FunctionConstructor} */
  entityClassToProvide;

  /** @type {Array<string>} */
  entityIndices;

  /**
   * @type {Function}
   * @returns {Promise<Array>}
   */
  getInitialCollectionFunction;

  constructor(fieldsValues) {
    utils.copyValuesInClassObjectProperties(fieldsValues, this);
  }
}

/**
 * Clase para obtener un recurso de una base de datos Local del navegador.
 * Uso:

 * ...
 *
 */
class CRUDEntityProviderLocalDb extends CRUDEntityProvider {
  /**
   * @private
   * @type {CRUDEntityProviderLocalDbConfig}
   */
  _config;

  /**
   * @private
   * @type {Loki}
   */
  static _localDb;

  /**
   * @private
   * @type {Collection}
   */
  _localEntityDbCollection;

  /**
   * @private
   * @type {boolean}
   */
  _isInsertedInitialCollectionData;

  /**
   * @private
   * @type {boolean}
   */
  _isLoadingInitialCollectionData;

  /**
   * Devuelve si la colección inicial está ya cargada en la persistencia del browser para ello carga también lo que hay en la persistencia
   * (es necesario para saber si en concreto esta entidad está en la persistencia del navegador)
   * @private
   * @returns {boolean}
   */
  async _checkAndLoadInitialCollectionDataFromBrowserPersistence() {
    let isCollectionInBrowserPersistence = false;

    if (this._isDbInBrowserPersistence()) {
      CRUDEntityProviderLocalDb._localDb.loadDatabase({}, err => {
        if (err) {
          throw new AppError("Error intentando cargar desde browser entidad " + this._config.entityClassNameToProvide, err);
        } else {
          // Si se trae de la persistencia ya se crea la "colessión" artomáticamente
          // console.log(
          //   `CRUDEntityProviderLocalDb: loading from browser persistente entidad ${this._config.entityClassNameToProvide}`,
          //   this._config.entityClassToProvide
          // );

          this._localEntityDbCollection = CRUDEntityProviderLocalDb._localDb.getCollection(this._config.entityClassNameToProvide);
          isCollectionInBrowserPersistence = !!this._localEntityDbCollection;
        }
      });
    }

    return isCollectionInBrowserPersistence;
  }

  /**
   * Devuelve una query que entienda lokijs porque hay que cambiar alguna sintaxis de operadores que lokijs no es iugal que mongodb
   * @private
   * @param {Object} query
   * @returns {Object}
   */
  _convertFilterToLokiQuery(query) {
    let lokiDbQuery = {};
    let operador = Object.keys(query)[0];

    if (operador) {
      let queryTerms = query[operador];
      let lokiDbQueryTerms = [];
      lokiDbQuery[operador] = lokiDbQueryTerms;

      queryTerms.forEach(queryTerm => {
        let newLokiQueryTerm = queryTerm;
        let queryTermName = Object.keys(queryTerm)[0];
        let queryTermValue = queryTerm[queryTermName];

        if (typeof queryTermValue === "object" && queryTermValue.$regex) {
          newLokiQueryTerm[queryTermName] = { $regex: [queryTermValue.$regex, queryTermValue.$options] };
        }

        lokiDbQueryTerms.push(newLokiQueryTerm);
      });
    }

    return lokiDbQuery;
  }

  /**
   * @private
   * @returns AxiosInstance
   */
  _createLocalDBWithCurrentConfig() {
    if (!CRUDEntityProviderLocalDb._localDb) {
      var persistentAdapter = new loki.LokiLocalStorageAdapter();

      CRUDEntityProviderLocalDb._localDb = new loki("AppLocalDb", {
        adapter: persistentAdapter
      });
    }
  }

  /**
   * @private
   * @returns {void}
   */
  async _ensureInitialCollectionDataIsInserted() {
    // TODO: Hacer un adapter en condiciones que controla todo esto
    if (!this._isInsertedInitialCollectionData && !this._isLoadingInitialCollectionData) {
      this._isLoadingInitialCollectionData = true;
      // La primera vez hay que cargar los datos en la localDb
      await this._loadInitialCollectionData();
    } else if (this._isLoadingInitialCollectionData && !this._isInsertedInitialCollectionData) {
      // esperamos, pq se está cargando por otro lado
      await utils.sleepWhile(() => this._isLoadingInitialCollectionData && !this._isInsertedInitialCollectionData);
    }
  }

  /**
   * @private
   * @return {object} Objecto query en formato mongodb https://docs.mongodb.com/manual/reference/operator/query/
   */
  _getFullTextSearchMongoDbQueryForAllEntityFields(fullTextSearch) {
    let mongodbQueryTerms = [];
    // debe quedar algo así como {$or: [{ nombre: { $regex: regexpValue } }, { apellido1: { $regex: regexpValue } }]}

    let firstObjectOfCollection = this._localEntityDbCollection.findOne({});

    // Obtenemos todos los campos para buscar por todos
    let filterFields = Object.getOwnPropertyNames(firstObjectOfCollection);

    filterFields.forEach(fieldName => {
      /* @type {Array|string} */
      // let regexpValue = utils.regexValueFromString(`/${this.fullTextSearch}/i`); // regexp busqueda de palabra en case INsensitive
      let termObject = {};
      termObject[fieldName] = { $regex: fullTextSearch, $options: "i" };

      mongodbQueryTerms.push(termObject);
    });

    return { $or: mongodbQueryTerms };
  }
  _isDbInBrowserPersistence() {
    return !!localStorage.getItem("AppLocalDb");
  }

  /**
   * @private
   * @returns {void}
   */
  async _loadInitialCollectionDataFromFunction() {
    let itemsArray = await this._config.getInitialCollectionFunction();

    // Inicializamos por primera vez la base de datos
    this._localEntityDbCollection = CRUDEntityProviderLocalDb._localDb.addCollection(this._config.entityClassNameToProvide, {
      indices: ["id"]
    });
    this._localEntityDbCollection.insert(itemsArray);

    // ssg: Modificado para que no se cargue en browser para así poder modificar citas.json
    // CRUDEntityProviderLocalDb._localDb.saveDatabase(err => {
    //   if (err) {
    //     throw new AppError("Error intentando persistir base de datos. Error:", err);
    //   } else {
    //     console.log(
    //     `CRUDEntityProviderLocalDb: _loadInitialCollectionDataFromFunction: database loaded: ${this._config.entityClassNameToProvide} `,
    //       this._config.entityClassToProvide
    //     );
    //   }
    // });
  }

  /**
   * @private
   * @returns {void}
   */
  async _loadInitialCollectionData() {
    let isInitialDataMustBeReadFromBrowserPersistence = await this._checkAndLoadInitialCollectionDataFromBrowserPersistence();

    if (!isInitialDataMustBeReadFromBrowserPersistence) {
      await this._loadInitialCollectionDataFromFunction();
    }

    this._isInsertedInitialCollectionData = true;
    this._isLoadingInitialCollectionData = false;
  }

  /** PUBLIC's  *************************/

  /**
   * Crea un CRUDEntityProvider inicializandolo con la configuración pasada
   * @param {CRUDEntityProviderLocalDbConfig} newConfig
   */
  constructor(newConfig) {
    super();
    this._config = newConfig;
    this._isInsertedInitialCollectionData = false;
    this._isLoadingInitialCollectionData = false;
    this._createLocalDBWithCurrentConfig();
  }

  /**
   * Devuelve el elementos del recurso (o entidad), cuando no se necesita buscar por id, pq solo hay un elemento de ese recurso,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   * @returns {Promise<Object>}
   */
  async readItem(id) {
    await this._ensureInitialCollectionDataIsInserted();

    let EntityClassToProvide = this._config.entityClassToProvide;

    let itemFieldValuesObject = this._localEntityDbCollection.findOne({ id });

    if (itemFieldValuesObject) {
      return new EntityClassToProvide(itemFieldValuesObject);
    } else {
      return null;
    }
  }

  /**
   * Devuelve un un objeto EntityList con el listado (e información de filtro, paginacion...) de elementos del recurso (o entidad) que
   * cumplen ciertos criterios
   * @param {EntityListParams=} listRequestParams
   * @returns {Promise<EntityList>}
   */
  async readList(listRequestParams) {
    // console.log("localDb:readlist", listRequestParams);
    await this._ensureInitialCollectionDataIsInserted();

    let filteredDbItemsResultSet = this._localEntityDbCollection.chain();

    if (listRequestParams && listRequestParams.filter && !listRequestParams.filter.fullTextSearch) {
      let lokiFilterQuery = this._convertFilterToLokiQuery(listRequestParams.filter.getFilterQuery());
      filteredDbItemsResultSet = filteredDbItemsResultSet.find(lokiFilterQuery);
    } else if (listRequestParams && listRequestParams.filter && listRequestParams.filter.fullTextSearch) {
      let lokiFilterQuery = this._convertFilterToLokiQuery(
        this._getFullTextSearchMongoDbQueryForAllEntityFields(listRequestParams.filter.fullTextSearch)
      );
      filteredDbItemsResultSet = filteredDbItemsResultSet.find(lokiFilterQuery);
    }

    let filteredDbItemsCount = filteredDbItemsResultSet.count();

    if (listRequestParams && listRequestParams.sort) {
      filteredDbItemsResultSet = filteredDbItemsResultSet.compoundsort([
        [listRequestParams.sort.fieldName, listRequestParams.sort.isDescending]
      ]);
    }

    if (listRequestParams && listRequestParams.paginationStart) {
      filteredDbItemsResultSet = filteredDbItemsResultSet.offset(
        (listRequestParams.paginationStart - 1) * listRequestParams.paginationLimit
      );
    }

    if (listRequestParams && listRequestParams.paginationLimit) {
      filteredDbItemsResultSet = filteredDbItemsResultSet.limit(listRequestParams.paginationLimit);
    }

    let filteredDbItemsList = filteredDbItemsResultSet.data();

    let entityListItems = filteredDbItemsList.map(filteredDbItem => {
      let entityListNewItem = utils.entityObjectFromFieldsValuesObject(filteredDbItem, this._config.entityClassToProvide);
      return entityListNewItem;
    });

    return new EntityList({
      items: entityListItems,
      itemsTotalCount: filteredDbItemsCount,
      params: listRequestParams
    });
  }
}

export { CRUDEntityProviderLocalDb, CRUDEntityProviderLocalDbConfig };
export default CRUDEntityProviderLocalDb;
