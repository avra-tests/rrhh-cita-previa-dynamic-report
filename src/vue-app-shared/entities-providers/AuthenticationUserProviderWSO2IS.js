import AppError from "../shared/AppError";
import utils from "../shared/utils";
import { AuthenticationUser, AuthenticationUserProvider } from "../shared/AuthenticationUser.js";
import axios from "axios";
import { AxiosInstance, AxiosPromise, AxiosError } from "axios";
import localforage from "localforage";

/**
 * Clase para authenticarse en WSO2 Identity Server
 * Uso:
 * 1.- Creación del provider
 * 2.- if !isLogged() {loginWithRedirect}
 * 3.- getAuthenticacionUserInfo
 * ...(para resto de peticiones) getAuthenticationTokens
 * ...
 *
 */
class AuthenticationUserProviderWSO2IS extends AuthenticationUserProvider {
  /**
   * Donde se van a guardar los valores en navegador para guarar tokens...
   * @private
   * @type {LocalForage}
   */
  _browserStorage;

  /**
   * @private
   *
   * @typedef {Object} WSO2ISConfig
   * @property {string} ws02ServerUrl
   * @property {string} clientId
   * @property {string} clientSecret
   * @property {string} redirectUrl
   */
  /** @type {WSO2ISConfig} */
  _currentConfig;

  /**
   * Instancia del cliente http para realizar peticiones http,
   * que en este caso concreto es axios
   * @private
   * @type AxiosInstance
   */
  _httpClient;

  /**
   * @private
   * @param newConfig {WSO2ISConfig}
   * @returns AxiosInstance
   */
  _createHttpClientwithConfig(newConfig) {
    return axios.create({
      baseURL: this._currentConfig.ws02ServerUrl
    });
  }

  /**
   * @private
   * @param {string} url
   */
  _getAuthorizationCodeFromQueryParams(url) {
    let currentUrl = window.location.href;

    return currentUrl.indexOf("code=") > -1 ? currentUrl.split("code=")[1].split("&")[0] : null;
  }

  /**
   * @private
   *
   * @typedef {Object} AuthenticationTokens
   * @property {string} accessToken
   * @property {string} refreshToken
   * @property {Date} expirationDate
   *
   * @returns {Promise<AuthenticationTokens>}
   */
  async _getBrowserStorageAuthenticationTokens() {
    let accessToken = await this._browserStorage.getItem("accessToken");
    let refreshToken = await this._browserStorage.getItem("refreshToken");
    let expirationDateJsonString = await this._browserStorage.getItem("expirationDateJSONString");
    // Esto hay que hacerlo así pq localforage no guarda Date (no es un objeto Jsonable según localdb usada en cada browser)
    let expirationDate = new Date(expirationDateJsonString);

    if (!(accessToken && refreshToken && expirationDate)) {
      return null;
    } else {
      return {
        accessToken: accessToken,
        refreshToken: refreshToken,
        expirationDate: expirationDate
      };
    }
  }

  async _getBrowserStorageAuthenticatedUser() {
    let authenticatedUser = await this._browserStorage.getItem("authenticatedUser");

    return authenticatedUser;
  }

  /**
   * @private
   * @returns {string}
   */
  _getWSO2IdentityServerLoginPageUrl() {
    const currentConfig = this._currentConfig;
    return (
      `${currentConfig.ws02ServerUrl}/oauth2/authorize` +
      `?response_type=code&scope=openid&client_id=${currentConfig.clientId}&redirect_uri=${currentConfig.redirectUrl}`
    );
  }

  /**
   * Devuelve si es valido el access token actual, para si no pedir otro
   * porque dará error de autenticación
   * @returns {Promise<boolean>}
   */
  async _isTimeToRefreshAccessToken() {
    let browserStorageAuthenticationTokens = await this._getBrowserStorageAuthenticationTokens();

    if (browserStorageAuthenticationTokens) {
      let moreSafeNowDate = new Date(Date.now() - 30000); // Le quitamos 30 segundos para que haya un poco de margen

      return moreSafeNowDate > browserStorageAuthenticationTokens.expirationDate;
    } else {
      return false;
    }
  }

  /**
   * Lee del Identity Server los tokens de autorización necesarios para posteriores peticiones de recursos http
   * @private
   * @param {string} authorizationCode
   * @returns AxiosPromise
   */
  _readAuthenticationTokensFromIdentityServer(authorizationCode) {
    let postData = new URLSearchParams();
    postData.append("grant_type", "authorization_code");
    postData.append("code", authorizationCode);
    postData.append(
      "redirect_uri",
      window.location.protocol + "//" + window.location.hostname + ":" + window.location.port + window.location.pathname
    );

    let authorizationIdsInBase64 = btoa(`${this._currentConfig.clientId}:${this._currentConfig.clientSecret}`);
    let requestAditionalConfig = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: `Basic ${authorizationIdsInBase64}`
      }
    };

    return this._httpClient.post("/oauth2/token/", postData, requestAditionalConfig);
  }

  /**
   * Obtiene los token refrescados (para cuando han expirado el access token)
   *
   * doc: https://is.docs.wso2.com/en/5.9.0/learn/refresh-token-grant/
   * ej seǵun doc: curl -k -d "grant_type=refresh_token&refresh_token=<refresh_token>"
   * -H "Authorization: Basic <Base64Encoded(Client_Id:Client_Secret)>"
   * -H "Content-Type: application/x-www-form-urlencoded" https://localhost:9443/oauth2/token
   *
   * @private
   * @throws {AxiosError}
   */
  async _readRefreshedTokenFromIdentityServer() {
    let browserStorageAuthenticationTokens = await this._getBrowserStorageAuthenticationTokens();
    let postData = new URLSearchParams();
    postData.append("grant_type", "refresh_token");
    postData.append("refresh_token", browserStorageAuthenticationTokens.refreshToken);

    let authorizationIdsInBase64 = btoa(`${this._currentConfig.clientId}:${this._currentConfig.clientSecret}`);
    let requestAditionalConfig = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: `Basic ${authorizationIdsInBase64}`
      }
    };

    return this._httpClient.post("/oauth2/token/", postData, requestAditionalConfig);
  }

  /**
   * @private
   * @param {string} accessToken
   */
  _readUserInfoFromIdentityServer(accessToken) {
    let requestAditionalConfig = {
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    };

    return this._httpClient.get("/oauth2/userinfo?schema=openid", requestAditionalConfig);
  }

  /**
   * @private
   * @param {AuthenticationTokens} newAuthenticationTokens
   */
  async _setBrowserStorageAuthenticationTokens(newAuthenticationTokens) {
    await this._browserStorage.setItem("accessToken", newAuthenticationTokens.accessToken);
    await this._browserStorage.setItem("refreshToken", newAuthenticationTokens.refreshToken);
    await this._browserStorage.setItem("expirationDateJSONString", newAuthenticationTokens.expirationDate.toJSON());
  }

  /**
   * @private
   */
  async _clearBrowserStorage() {
    await this._browserStorage.clear();
  }

  /**
   * @private
   * @param {Object} authenticatedUser
   */
  async _setBrowserStorageAuthenticatedUser(authenticatedUser) {
    await this._browserStorage.setItem("authenticatedUser", authenticatedUser);
  }

  /** PUBLIC's  *************************/

  /**
   * Crea un AuthenticationProvider inicializandolo con la configuración pasada
   * @param {WSO2ISConfig} newConfig
   */
  constructor(newConfig) {
    super();

    this._currentConfig = utils.cloneObjectValues(newConfig);
    this._httpClient = this._createHttpClientwithConfig(newConfig);
    this._browserStorage = localforage.createInstance({
      name: "AuthenticationUserProviderWSO2ISStorage"
    });
  }

  /**
   * Devuelve los tokens de autenticación necesarios para realizar peticiones http autenticadas
   * Se encarga automágicamente de ir refrescando el token según se necesite
   *
   * @returns {Promise<AuthenticationTokens>}
   */
  async getAuthenticationTokens() {
    let browserStorageAuthenticationTokens = await this._getBrowserStorageAuthenticationTokens();

    let authorizationCode = this._getAuthorizationCodeFromQueryParams(window.location.href);

    if (!browserStorageAuthenticationTokens && authorizationCode) {
      // No se han guardado los tokens todavía luego no nos hemos autenticado con este navegado y tenemos que venir de la pantalla de oauth,
      // que redirecciona a la app de nuevo y se recibe algo así: http://localhost:8081/?code=7092c9dd...
      // si no es así no hay tokens posibles de autenticación...
      let authenticationTokensResponse = await this._readAuthenticationTokensFromIdentityServer(authorizationCode);

      let expirationDate = new Date(Date.now() + authenticationTokensResponse.data["expires_in"] * 1000);

      await this._setBrowserStorageAuthenticationTokens({
        accessToken: authenticationTokensResponse.data["access_token"],
        refreshToken: authenticationTokensResponse.data["refresh_token"],
        expirationDate: expirationDate
      });
    }

    let isTimeToRefreshAccessToken = await this._isTimeToRefreshAccessToken();

    if (isTimeToRefreshAccessToken) {
      // En este punto hay browserStorageAuthenticationTokens guardados

      try {
        let refreshedTokensResponse = await this._readRefreshedTokenFromIdentityServer();

        // Si el refresh token es caducado o ha sido revocado se obtiene un error
        // y se obtiene algo así: {"error_description":"Persisted access token data not found","error":"invalid_grant"}

        let expirationDate = new Date(Date.now() + refreshedTokensResponse.data["expires_in"] * 1000);

        await this._setBrowserStorageAuthenticationTokens({
          accessToken: refreshedTokensResponse.data["access_token"],
          refreshToken: refreshedTokensResponse.data["refresh_token"],
          expirationDate: expirationDate
        });
      } catch (error) {
        // Si hay algún error es que ya no es válido el refresh token y borramos la autenticación que teníamos guaradada
        // pq hay que obtener nueva autorizacion desde el principio, con el login
        await this._clearBrowserStorage();
      }
    }

    browserStorageAuthenticationTokens = await this._getBrowserStorageAuthenticationTokens();
    return browserStorageAuthenticationTokens;
  }

  /**
   * @returns {Promise<AuthenticationUser>}
   */
  async getAuthenticatedUser() {
    let authenticationTokens = await this.getAuthenticationTokens();
    let authenticatedUser = await this._getBrowserStorageAuthenticatedUser();

    if (authenticatedUser && authenticationTokens) {
      // Lo tenemos guardado de antes (y además no ha caducado el token)
      // Nos ahorramos consultarlo otra vez al servidor
      return authenticatedUser;
    } else {
      // Si no lo tenemos guardado, hay que pedirlo al servidor
      let userInfoResponse = await this._readUserInfoFromIdentityServer(authenticationTokens.accessToken);

      let userPhotoUrl = null;

      if (userInfoResponse.data.sub && userInfoResponse.data.sub === "ssg") {
        userPhotoUrl = "http://repofotos.epsa.junta-andalucia.es/fotosListin/000496.jpg";
      } else if (userInfoResponse.data.sub && userInfoResponse.data.sub === "jalr") {
        userPhotoUrl = "http://repofotos.epsa.junta-andalucia.es/fotosListin/000766.jpg";
      }

      authenticatedUser = new AuthenticationUser({
        id: userInfoResponse.data.sub,
        nombre: userInfoResponse.data.given_name,
        apellidos: userInfoResponse.data.family_name,
        photoUrl: userPhotoUrl,
        dni: userInfoResponse.data.address ? userInfoResponse.data.address.address : null // lo devuelve así (raro, raro... )
      });

      await this._setBrowserStorageAuthenticatedUser(authenticatedUser);
    }

    return authenticatedUser;
  }

  /**
   * @returns {Promise<boolean>} Devuelve true si ya estamos logado (para saber si hay que hace ro no login)
   */
  async isLogged() {
    let browserStorageAuthenticationTokens = await this.getAuthenticationTokens();

    return !!browserStorageAuthenticationTokens;
  }

  /**
   * Redirecciona a la pantalla de autenticación del proveedor
   * Después el proveedor reenviará al navegador a la url de redireccion que le digamos
   * @returns {void}
   */
  loginWithRedirect() {
    utils.redirectBrowserToPage(this._getWSO2IdentityServerLoginPageUrl());
  }
}

export default AuthenticationUserProviderWSO2IS;
