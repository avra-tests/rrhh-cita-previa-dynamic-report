import CRUDEntityProviderQmatic from "./CRUDEntityProviderQmatic";
import { EntityList, EntityListParams } from "../shared/EntityList";
import utils from "../shared/utils";

import PerfilCita from "src/store/citas/Cita";

/**
 * Rest service de QMatic devuelve los datos de manera que no nos interesa, aquí los adaptamos a algo más simple
 */
export default class CRUDEntityProviderQmaticPerfilCita extends CRUDEntityProviderQmatic {
  /** PUBLIC  ****/
  /**
   * @param {CRUDEntityProviderQmaticConfig} newConfig
   */
  constructor(newConfig) {
    // newConfig.responseToEntityListFieldsValuesFunction = _qmaticResponseToCitaEntityListFieldsValuesFunction;
    // newConfig.responseToEntityListFieldsValuesFunction = this.prototype._qmaticResponseToCitaEntityListFieldsValuesFunction;
    super(newConfig);
  }

  /** PRIVATE *****/
  /**
   * @private
   *
   * @param {*} response
   * @param {*} entityClassToProvide
   * @param {EntityListParams} listParams
   *
   * @returns {EntityList}
   */
  _responseToEntityFunction(response, entityClassToProvide, listParams) {
    let entityListItems = [];
    let responseListItems = [];

    responseListItems = response.data.appointmentprofileList;
    // esto es pq lo servicios entregados no lo devuelven nada por ahora
    if (!response.headers["X-Total-Count"]) {
      // TODO: cambiar cuando esté el servicio guay
      response.headers["X-Total-Count"] = responseListItems.length;
    }

    entityListItems = responseListItems
      .map(dataItem => this._perfilCitaEntityObjectFromDataItem(dataItem));

    /** @type {EntityList} */
    let list = new EntityList({
      items: entityListItems,
      itemsTotalCount: response.headers["X-Total-Count"],
      params: listParams
    });

    return list;
  }

  /**
   *
   * @param {object} fieldsValuesObject
   * @param {FunctionConstructor} EntityClass
   */
  _perfilCitaEntityObjectFromDataItem(dataItem) {
    let entityListNewItem = {};

    entityListNewItem = {
      id: dataItem.id,
      nombre: dataItem.nombre ?? null,
      duracion: dataItem.duracion ?? null,
      diasReservables: dataItem.diasReservables ?? null,
      planificacionSemanal: dataItem.planificacionSemanal ?? null,
      oficina: dataItem.oficina ?? null,
      servicios: dataItem.servicios ?? [],
      recursos: dataItem.recursos ?? []
    };

    const isDataItemFromRestService = !!(dataItem.services && dataItem.services[0]);

    if (isDataItemFromRestService) {
      entityListNewItem.nombre = dataItem.name ?? null;
      entityListNewItem.duracion = dataItem.interval ?? null;
      entityListNewItem.diasReservables = dataItem.bookingPeriodDays ?? null;
      entityListNewItem.planificacionSemanal = dataItem.capacity ?? null;
      entityListNewItem.oficina = dataItem.branch ?? null;
      entityListNewItem.servicios = dataItem.services ?? [];
      entityListNewItem.recursos = dataItem.resourceGroup && dataItem.resourceGroup.resources ? dataItem.resourceGroup.resources : [];
    }

    return entityListNewItem;
  }
}
