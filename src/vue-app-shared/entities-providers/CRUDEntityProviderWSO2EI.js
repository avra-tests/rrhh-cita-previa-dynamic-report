import AppError from "../shared/AppError";
import AuthenticationUserProviderWSO2IS from "../entities-providers/AuthenticationUserProviderWSO2IS";
import { EntityList, EntityListParams } from "../shared/EntityList";
import { CRUDEntity, CRUDEntityProvider } from "../shared/CRUDEntity";

import utils from "../shared/utils";

import axios from "axios";
import { AxiosInstance, AxiosPromise, AxiosError, AxiosResponse, AxiosRequestConfig } from "axios";

class CRUDEntityProviderWSO2EIError extends AppError {
  constructor(msg, currentResourceConfig) {
    let newMsg = `RestResourceWSO2EIProvider: ${msg}. Config: ${currentResourceConfig}`;
    super(newMsg);
  }
}

class CRUDEntityProviderWSO2EIConfig {
  /** @type {AuthenticationUserProviderWSO2IS} */
  authenticationUserProvider;

  /** @type {CRUDEntityProvider} */
  authenticationUserPermissionsProvider;

  /** @type  {string} */
  restServerUrl;

  /** @type  {string} */
  apiBasePath;

  /** @type  {string} */
  resourceName;

  /** @type {FunctionConstructor} */
  entityClassToProvide;

  /** @type {any}*/
  localDB;

  /** @type  {Function} */
  responseToEntityFieldsValuesFunction;

  /** @type  {Function} */
  responseToEntityListFieldsValuesFunction;

  constructor(fieldsValues) {
    utils.copyValuesInClassObjectProperties(fieldsValues, this);
  }
}

/**
 *
 * @param {*} response
 * @param {*} entityClassToProvide
 * @param {EntityListParams} listParams
 */
const rawJsonArrayResponseToEntityListValuesFunction = function(response, entityClassToProvide, listParams) {
  let responseListItems = [];

  try {
    // Esto es porque el response viene con p.e { "Empleados": "[]"}
    //responseListItems = utils.getFirstFieldValueOfObject(response.data.data);
    // Cambiado por JALR
    responseListItems = response.data.data;
    let totalCount = response.data.countTotal;
    response.headers["X-Total-Count"] = totalCount;
    // esto es pq lo servicios entregados no lo devuelven nada por ahora
    if (!response.headers["X-Total-Count"]) {
      // TODO: cambiar cuando esté el servicio guay
      response.headers["X-Total-Count"] = responseListItems.length;
    }
  } catch (error) {
    throw new AppError(`Error conviertiendo response a entidad ${utils.getClassName(entityClassToProvide)}, excepcion: ${error}`);
  }

  /** @type {EntityList} */
  let list = new EntityList({
    items: responseListItems,
    itemsTotalCount: response.headers["X-Total-Count"],
    params: listParams
  });

  return list;
};

const rawJsonItemResponseToEntityValuesFunction = function(response, entityClassToProvide) {
  let itemResponse = null;
  try {
    itemResponse = response.data;
  } catch (error) {
    throw new AppError(`Error conviertiendo response a entidad ${utils.getClassName(entityClassToProvide)}, excepcion: ${error}`);
  }
  return new entityClassToProvide(itemResponse);
};

/**
 * Clase para obtener una recurso rest de WSO2 Enterprise Integrator.
 * Sirve como base para obtener el resto de providers de Entities
 * Uso:

 * ...
 *
 */
class CRUDEntityProviderWSO2EI extends CRUDEntityProvider {
  /**
   * @private
   * @type {CRUDEntityProviderWSO2EIConfig}
   */
  _config;

  /**
   * Instancia del cliente http para realizar peticiones http,
   * que en este caso concreto es axios
   * @private
   * @type AxiosInstance
   */
  _httpClient;

  /**
   * @private
   * @returns AxiosInstance
   */
  _createHttpClientwithCurrentConfig() {
    let baseUrl4requests = `${this._config.restServerUrl}${this._config.apiBasePath}`;

    return axios.create({
      baseURL: baseUrl4requests
    });
  }

  /**
   * @private
   * @returns {Promise<Object>}
   */
  async _getAuthorizationHeaderForHttpRequest() {
    let addtionalHeaders = {};

    if (this._config.authenticationUserProvider) {
      let authenticationTokens = await this._config.authenticationUserProvider.getAuthenticationTokens();

      addtionalHeaders = {
        "Content-Type": "application/json",
        token: authenticationTokens.accessToken
      };
    }

    return addtionalHeaders;
  }

  /**
   * @returns {Promise<Object>} parametros que hay que añadir a la peticion http
   */
  async _getAuthorizationParamsForHttpRequest() {
    let params = {};

    if (this._config.authenticationUserPermissionsProvider) {
      let permissions = await this._config.authenticationUserPermissionsProvider.readItem();
      params.idFuncion = permissions.usuario.idFuncion;
    }

    return params;
  }

  /**
   * @private
   * @param {string} method
   * @param {object=} additionalRequestParams Opcional, parametros adicionales del request, como p.e. los de filtro...
   * @returns {Promise<Object>}
   */
  async _doHttpClientCommonRequest(method, additionalRequestParams, pathParam) {
    // Hay que pedir los headers de autorizacion en cada peticion porque el token de acceso se va refrescando
    let additionalConfigForAutorizationHeaders = await this._getAuthorizationHeaderForHttpRequest();
    let additionalParamsForUserPermissions = await this._getAuthorizationParamsForHttpRequest();

    let resourceName = this._config.resourceName;

    //llamadas resource/id
    if (pathParam) {
      resourceName = resourceName + "/" + pathParam;
    }

    let requestConfig = {
      url: resourceName,
      method: method,
      headers: { ...additionalConfigForAutorizationHeaders },
      params: {
        ...additionalParamsForUserPermissions,
        ...additionalRequestParams
      }
    };
    // console.log("_doHttpClientCommonRequest", requestConfig);
    return this._httpClient.request(requestConfig);
  }

  /**
   * @private
   * @param {string} method
   * @param {object=} item Obejto a modificar o crear
   * @returns {Promise<Object>}
   */
  async _doHttpClientCommonUpdate(method, item) {
    // Hay que pedir los headers de autorizacion en cada peticion porque el token de acceso se va refrescando
    let additionalConfigForAutorizationHeaders = await this._getAuthorizationHeaderForHttpRequest();
    let additionalParamsForUserPermissions = await this._getAuthorizationParamsForHttpRequest();

    let resourceName = this._config.resourceName;

    let requestConfig = {
      url: resourceName,
      method: method,
      data: item,
      headers: { ...additionalConfigForAutorizationHeaders }
    };
    //console.log("_doHttpClientCommonUpdate", requestConfig);
    return this._httpClient.request(requestConfig);
  }

  /**
   * Transforma un response a un objeto de la entidad que corresponda aplicando la funcion pasada
   * @param {AxiosResponse} response
   * @param {Function} responseToEntityFunction
   * @param {object=} extraParams Parametros extra, como p.e. params de un listado
   */
  _responseToEntityObject(response, responseToEntityFunction, extraParams) {
    // console.log ("_responseToEntityObject - Function:", responseToEntityFunction)
    let EntityClassToProvide = this._config.entityClassToProvide;
    let entityObject = responseToEntityFunction(response, EntityClassToProvide, extraParams);
    // console.log ("_responseToEntityObject:", entityObject)
    return entityObject;
  }

  // _checkErrorsInGetResponseDataType(response, expectedDataType) {
  //   if (!expectedDataType || !expectedDataType.name) {
  //     throw new CRUDEntityProviderWSO2EIError("No se ha pasado ningún tipo esperado para response.data", this._config);
  //   } else if (!response.data.datos) {
  //     throw new CRUDEntityProviderWSO2EIError(
  //       "No se ha recibido el data del response como se esperada: response.data.datos",
  //       this._config
  //     );
  //   } else if (expectedDataType.name === "Array" && response.data.datos[0][0] instanceof Array) {
  //     throw new CRUDEntityProviderWSO2EIError(
  //       "Se esperaba un Array en response.data.datos.xxx.xxx, y ha llegado otra cosa",
  //       this._config
  //     );
  //   }
  // }

  /** PUBLIC's  *************************/

  /**
   * Crea un AuthenticationProvider inicializandolo con la configuración pasada
   * @param {CRUDEntityProviderWSO2EIConfig} newConfig
   */
  constructor(newConfig) {
    super();
    this._config = newConfig;
    this._config.responseToEntityListFieldsValuesFunction = rawJsonArrayResponseToEntityListValuesFunction;
    this._config.responseToEntityFieldsValuesFunction = rawJsonItemResponseToEntityValuesFunction;
    this._httpClient = this._createHttpClientwithCurrentConfig();
  }

  /**
   * Devuelve el elementos del recurso (o entidad), cuando no se necesita buscar por id, pq solo hay un elemento de ese recurso,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   *
   * @returns {Promise<Object>}
   */
  async readItem(id) {
    let response = await this._doHttpClientCommonRequest("get", null, id);
    return this._responseToEntityObject(response, this._config.responseToEntityFieldsValuesFunction);
  }

  /**
   * Crea un nuevo regisgtro con los datos y
   * Devuelve el elementos del recurso (o entidad) actualizado,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   * @param {CRUDEntity=} item objeto que se desea actualizar
   * @returns {Promise<Object>}
   */
  async createItem(item) {
    let response = await this._doHttpClientCommonUpdate("post", item);
    return this._responseToEntityObject(response, this._config.responseToEntityFieldsValuesFunction);
  }

  /**
   * Actualiza la totalidad de los datos y
   * Devuelve el elementos del recurso (o entidad) actualizado,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   * @param {CRUDEntity=} item objeto que se desea actualizar
   * @returns {Promise<Object>}
   */
  async updateItem(item) {
    let response = await this._doHttpClientCommonUpdate("put", item);
    return this._responseToEntityObject(response, this._config.responseToEntityFieldsValuesFunction);
  }

  /**
   * Actualiza solo los campos que se reciben en el objeto correspondiente
   * Devuelve el elementos del recurso (o entidad) actualizado,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   * @param {Object=} objetct objeto Json que se desea actualizar.
   * @returns {Promise<Object>}
   */
  async patchItem(object) {
    let response = await this._doHttpClientCommonUpdate("patch", object);
    return this._responseToEntityObject(response, this._config.responseToEntityFieldsValuesFunction);
  }

  /**
   * Devuelve un un objeto EntityList con el listado (e información de filtro, paginacion...) de elementos del recurso (o entidad)
   * que cumplen ciertos criterios
   * @param {EntityListParams=} listRequestParams parametros de peticion del listado, si no se indica se pide todo el listado
   * @returns {Promise<EntityList>}
   */
  async readList(listRequestParams) {
    // console.log("readList", listRequestParams);
    let queryParams = {};
    let filterQuery = listRequestParams && listRequestParams.filter ? listRequestParams.filter.getFilterQuery() : null;
    // console.log("readList - FIlterQuery", filterQuery);

    if (filterQuery !== null && Object.entries(filterQuery).length > 0) {
      queryParams["_filter"] = filterQuery;
    }

    if (listRequestParams && listRequestParams.paginationStart) {
      queryParams["_start"] = listRequestParams.paginationStart;
    }

    if (listRequestParams && listRequestParams.paginationLimit) {
      queryParams["_limit"] = listRequestParams.paginationLimit;
    }

    if (listRequestParams && listRequestParams.sort && Object.entries(listRequestParams.sort).length > 0) {
      let order = {};
      let fieldName = listRequestParams.sort.fieldName;
      let campo = {};
      campo[fieldName] = listRequestParams.sort.isDescending ? "-1" : "1";
      order["$orderby"] = campo;
      queryParams["_sort"] = order;
    }

    let response = await this._doHttpClientCommonRequest("get", queryParams);
    return this._responseToEntityObject(response, this._config.responseToEntityListFieldsValuesFunction, listRequestParams);
  }
}

export { CRUDEntityProviderWSO2EI, CRUDEntityProviderWSO2EIError, CRUDEntityProviderWSO2EIConfig };
export default CRUDEntityProviderWSO2EI;
