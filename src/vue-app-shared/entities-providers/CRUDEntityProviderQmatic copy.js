import AppError from "../shared/AppError";
import { EntityList, EntityListParams } from "../shared/EntityList";
import { CRUDEntity, CRUDEntityProvider } from "../shared/CRUDEntity";
import utils from "../shared/utils";

import axios from "axios";
import { AxiosInstance, AxiosPromise, AxiosError, AxiosResponse, AxiosRequestConfig } from "axios";

import Cita from "src/store/citas/Cita";

class CRUDEntityProviderQmaticError extends AppError {
  constructor(msg, currentResourceConfig) {
    let newMsg = `RestResourceQmaticProvider: ${msg}. Config: ${currentResourceConfig}`;
    super(newMsg);
  }
}

class CRUDEntityProviderQmaticConfig {
  /** @type  {string} */
  restServerUrl;

  /** @type  {string} */
  apiBasePath;

  /** @type  {string} */
  resourceName;

  /** @type {FunctionConstructor} */
  entityClassToProvide;

  /** @type  {Function} */
  responseToEntityFieldsValuesFunction;

  /** @type  {Function} */
  responseToEntityListFieldsValuesFunction;

  constructor(fieldsValues) {
    utils.copyValuesInClassObjectProperties(fieldsValues, this);
  }
}

/**
 *
 * @param {object} fieldsValuesObject
 * @param {FunctionConstructor} EntityClass
 */
function citaEntityObjectFromDataItem(dataItem) {
  let entityListNewItem = {};

  entityListNewItem = {
    id: dataItem.id,
    titulo: dataItem.title ?? null,
    fecha: dataItem.start ?? null,
    fechaFin: dataItem.end ?? null,
    nombre: dataItem.nombre ?? null,
    apellidos: dataItem.apellidos ?? null,
    email: dataItem.email ?? null,
    telefono: dataItem.telefono ?? null,
    nif: dataItem.nif ?? null,
    oficina: dataItem.oficina ?? null,
    servicio: dataItem.servicio ?? null,
    recurso: dataItem.recurso ?? null,
    numeroCita: dataItem.numeroCita ?? null,
    duracion: dataItem.duracion ?? null,
    tipo: dataItem.tipo ?? null
  };

  const isDataItemFromRestService = !!(dataItem.customers && dataItem.customers[0]);

  if (isDataItemFromRestService) {
    entityListNewItem.nombre = dataItem.customers[0].firstName ?? null;
    entityListNewItem.apellidos = dataItem.customers[0].lastName ?? null;
    entityListNewItem.email = dataItem.customers[0].email ?? null;
    entityListNewItem.telefono = dataItem.customers[0].phone ?? null;
    entityListNewItem.nif = dataItem.customers[0].externalId
      ? dataItem.customers[0].externalId !== "11111111H"
        ? dataItem.customers[0].externalId
        : null
      : null;
    entityListNewItem.oficina = dataItem.branch.name ?? null;
    entityListNewItem.servicio = dataItem.services && dataItem.services[0] ? dataItem.services[0].name : null;
    entityListNewItem.recurso = dataItem.resource.name ?? null;
    entityListNewItem.duracion = dataItem.services && dataItem.services[0] ? dataItem.services[0].duration : null;
    entityListNewItem.tipo = entityListNewItem.duracion ? "Reservado" : "No disponible";
    entityListNewItem.numeroCitas = entityListNewItem.duracion ? 1 : 0;
  }

  return entityListNewItem;
}

/**
 *
 * @param {Cita} cita
 */
function isCitaConOficina(cita) {
  return cita.oficina && cita.oficina !== "Prueba";
}

/**
 *
 * @param {Array<Cita>} entityListItemsCitas
 */
function sortCitasListByOficinaRecursosFecha(entityListItemsCitas) {
  let listItemsSorted = entityListItemsCitas.sort((cita1, cita2) => {
    let compareOficina = utils.compareStrings(cita1.oficina, cita2.oficina);
    let compareRecursos = utils.compareStrings(cita1.recurso, cita2.recurso);
    let compareFechasHoras = utils.compareStrings(cita1.fecha, cita2.fecha);

    if (compareOficina !== 0) {
      return compareOficina;
    } else if (compareRecursos !== 0) {
      return compareRecursos;
    } else {
      return compareFechasHoras;
    }
  });
  // console.log("listItemsSorted", listItemsSorted);
  return listItemsSorted;
}

/**
 *
 * @param {*} previousTimeToCita
 * @param {*} currentCita
 * @param {*} fecha
 *
 * @returns {number}
 */
function getMinutesBetweenDates(date2, date1) {
  return Math.trunc((date2 - date1) / 60000);
}

function isDateTimeStringsSameDay(dateString1, dateString2) {
  return new Date(dateString1).toDateString() === new Date(dateString2).toDateString();
}

/**
 *
 * @param {Date} date
 *
 * @returns {string} Devuelve algo así '2021-02-10T23:00:00.000+0100'
 */
function toISODateTimeStringWithGmtFromDate(date) {
  return new Date(date.getTime() - date.getTimezoneOffset() * 60000).toISOString().replace("Z", "+0000");
}

/**
 *
 * @param {Cita} cita
 */
function getFirstHourDateForCita(cita) {
  return new Date(new Date(cita.fecha).setHours(8, 0, 0, 0));
}

/**
 *
 * @param {Cita} cita
 */
function getLastHourDateTimeStringForCita(cita) {
  return new Date(new Date(cita.fecha).setHours(18, 0, 0, 0));
}

/**
 * @param {*} currentCita
 * @param {*} index
 * @param {*} citasList
 *
 * @returns Date
 */
function previousDateToCita(currentCita, index, citasList) {
  let previousDateToCita = null;

  if (index === 0) {
    previousDateToCita = getFirstHourDateForCita(currentCita);
  } else if (!isDateTimeStringsSameDay(currentCita.fecha, citasList[index - 1].fecha)) {
    previousDateToCita = getFirstHourDateForCita(currentCita);
  } else {
    previousDateToCita = new Date(citasList[index - 1].fechaFin);
  }

  return previousDateToCita;
}

/**
 *
 * @param {Array<Cita>} entityListItemsCitas
 */
function addAndSortHuecosSinClientes(entityListItemsCitas) {
  let citasListSorted = sortCitasListByOficinaRecursosFecha(entityListItemsCitas);
  let citasHuecosList = [];

  citasListSorted.forEach((currentCita, index, citasList) => {
    let currentCitaDate = new Date(currentCita.fecha);
    let previousDate = previousDateToCita(currentCita, index, citasList);
    let previousDateString = toISODateTimeStringWithGmtFromDate(previousDate);

    // console.log("addAndSortHuecosSinClientes", { previousDate, currentCita, index });

    // let nextDateStringToCita = nextDateStringToCita(currentCita, index, citasList);

    if (previousDate < currentCitaDate) {
      citasHuecosList.push(
        new Cita({
          id: null,
          titulo: null,
          fecha: previousDateString,
          fechaFin: currentCita.fecha,
          nombre: null,
          apellidos: null,
          email: null,
          telefono: null,
          nif: null,
          oficina: currentCita.oficina ?? null,
          servicio: null,
          recurso: currentCita.recurso ?? null,
          numeroCita: 1,
          duracion: getMinutesBetweenDates(currentCitaDate, previousDate),
          tipo: "Libre"
        })
      );
      // console.log("citasHuecosList: previous", previousDateString, previousDate );
      // console.log("citasHuecosList:     cita",  currentCita.fecha,  currentCitaDate);
      // console.log("----", getMinutesBetweenDates(currentCitaDate, previousDate));
    }
    //  else if (nextTimeToCita > currentCita.fecha) {
    //     citasHuecosList.push( new Cita ({
    //         id: null,
    //         titulo: null,
    //         fecha: currentCita.fecha,
    //         fechaFin: nextTimeToCita,
    //         nombre: null,
    //         apellidos: null,
    //         email: null,
    //         telefono: null,
    //         nif: null,
    //         oficina: currentCita.oficina ?? null,
    //         servicio: null,
    //         recurso: currentCita.recurso ?? null,
    //         numeroCita: 1,
    //         duracion: getMinutesBetweenDateStrings(currentCita.fecha, nextDateStringToCita),
    //         tipo: "Libre"
    //     }));
    // }
  });

  return sortCitasListByOficinaRecursosFecha([...citasListSorted, ...citasHuecosList]);
}

/**
 *
 * @param {*} response
 * @param {*} entityClassToProvide
 * @param {EntityListParams} listParams
 */
const rawJsonArrayResponseToEntityListValuesFunction = function(response, entityClassToProvide, listParams) {
  let entityListItems = [];

  // try {
  let responseListItems = [];
  responseListItems = response.data.appointmentList;

  // esto es pq lo servicios entregados no lo devuelven nada por ahora
  if (!response.headers["X-Total-Count"]) {
    // TODO: cambiar cuando esté el servicio guay
    response.headers["X-Total-Count"] = responseListItems.length;
  }

  switch (entityClassToProvide.name) {
    case "Cita":
      let citasEnetityListItems = responseListItems
        .map(dataItem => citaEntityObjectFromDataItem(dataItem))
        .filter(cita => isCitaConOficina(cita));

      entityListItems = addAndSortHuecosSinClientes(citasEnetityListItems);

      //.filter(cita => isCitaValida(cita));

      break;

    default:
      throw new AppError(`No EntityObjectFromDataItem para clases ${entityClassToProvide}`);
  }
  // } catch (error) {
  //   throw new AppError(`Error conviertiendo response a entidad ${response}, excepcion: ${error}`);
  // }

  /** @type {EntityList} */
  let list = new EntityList({
    items: entityListItems,
    itemsTotalCount: response.headers["X-Total-Count"],
    params: listParams
  });

  return list;
};

/**
 * Clase para obtener una recurso rest de un fichero json
 * Uso:
 * ...
 *
 */
class CRUDEntityProviderQmatic extends CRUDEntityProvider {
  /**
   * @private
   * @type {CRUDEntityProviderQmaticConfig}
   */
  _config;

  /**
   * Instancia del cliente http para realizar peticiones http,
   * que en este caso concreto es axios
   * @private
   * @type AxiosInstance
   */
  _httpClient;

  /**
   * @private
   * @returns AxiosInstance
   */
  _createHttpClientwithCurrentConfig() {
    let baseUrl4requests = `${this._config.restServerUrl}${this._config.apiBasePath}`;

    return axios.create({
      baseURL: baseUrl4requests
    });
  }

  /**
   * @private
   * @returns {Promise<Object>}
   */
  async _getAuthorizationHeaderForHttpRequest() {
    let additionalHeaders = {};

    // TODO: Hacer esto con un authentication basic provider
    const token = `integracion:Integracion15`;
    const encodedToken = Buffer.from(token).toString("base64");

    additionalHeaders = { Authorization: "Basic " + encodedToken };

    return additionalHeaders;
  }

  /**
   * @returns {Promise<Object>} parametros que hay que añadir a la peticion http
   */
  async _getAuthorizationParamsForHttpRequest() {
    let params = {};

    // if (this._config.authenticationUserPermissionsProvider) {
    //   let permissions = await this._config.authenticationUserPermissionsProvider.readItem();
    //   params.idFuncion = permissions.usuario.idFuncion;
    // }

    return params;
  }

  /**
   * @private
   * @param {string} method
   * @param {object=} additionalRequestParams Opcional, parametros adicionales del request, como p.e. los de filtro...
   * @returns {Promise<Object>}
   */
  async _doHttpClientCommonRequest(method, additionalRequestParams) {
    // Hay que pedir los headers de autorizacion en cada peticion porque el token de acceso se va refrescando
    let additionalConfigForAutorizationHeaders = await this._getAuthorizationHeaderForHttpRequest();
    let additionalParamsForUserPermissions = await this._getAuthorizationParamsForHttpRequest();

    let requestConfig = {
      url: this._config.resourceName,
      method: method,
      headers: { ...additionalConfigForAutorizationHeaders },
      params: {
        ...additionalParamsForUserPermissions,
        ...additionalRequestParams
      }
    };

    return this._httpClient.request(requestConfig);
  }

  /**
   * Transforma un response a un objeto de la entidad que corresponda aplicando la funcion pasada
   * @param {AxiosResponse} response
   * @param {Function} responseToEntityFunction
   * @param {object=} extraParams Parametros extra, como p.e. params de un listado
   */
  _responseToEntityObject(response, responseToEntityFunction, extraParams) {
    let EntityClassToProvide = this._config.entityClassToProvide;
    let entityObject = responseToEntityFunction(response, EntityClassToProvide, extraParams);

    return entityObject;
  }

  /** PUBLIC's  *************************/

  /**
   * Crea un AuthenticationProvider inicializandolo con la configuración pasada
   * @param {CRUDEntityProviderQmaticConfig} newConfig
   */
  constructor(newConfig) {
    super();
    this._config = newConfig;
    this._config.responseToEntityListFieldsValuesFunction = rawJsonArrayResponseToEntityListValuesFunction;
    this._httpClient = this._createHttpClientwithCurrentConfig();
  }

  /**
   * Devuelve el elementos del recurso (o entidad), cuando no se necesita buscar por id, pq solo hay un elemento de ese recurso,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   *
   * @returns {Promise<Object>}
   */
  async readItem() {
    let response = await this._doHttpClientCommonRequest("get");

    return this._responseToEntityObject(response, this._config.responseToEntityFieldsValuesFunction);
  }

  /**
   * Devuelve un un objeto EntityList con el listado (e información de filtro, paginacion...) de elementos del recurso (o entidad)
   * que cumplen ciertos criterios
   * @param {EntityListParams=} listRequestParams parametros de peticion del listado, si no se indica se pide todo el listado
   * @returns {Promise<EntityList>}
   */
  async readList(listRequestParams) {
    // console.log("readList");

    let response = await this._doHttpClientCommonRequest("get", listRequestParams);
    return this._responseToEntityObject(response, this._config.responseToEntityListFieldsValuesFunction, listRequestParams);
  }
}

export { CRUDEntityProviderQmatic, CRUDEntityProviderQmaticError, CRUDEntityProviderQmaticConfig };
export default CRUDEntityProviderQmatic;
