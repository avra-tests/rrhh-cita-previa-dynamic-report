import AppConfiguration from "./shared/AppConfiguration";
import AppError from "./shared/AppError";
import { CRUDEntity, CRUDEntityProvider } from "./shared/CRUDEntity";
import { EntityList, EntityListParams } from "./shared/EntityList";
import { Filter, FilterFieldType, FilterOperator } from "./shared/Filter";
import { AuthenticationUser, AuthenticationUserProvider } from "./shared/AuthenticationUser";
import AuthenticationUserPermissions from "./shared/AuthenticationUserPermissions";
import AuthenticationUserProviderWSO2IS from "./entities-providers/AuthenticationUserProviderWSO2IS";
import { CRUDEntityProviderJSONFile, CRUDEntityProviderJSONFileConfig } from "./entities-providers/CRUDEntityProviderJSONFile";
import { CRUDEntityProviderLocalDb, CRUDEntityProviderLocalDbConfig } from "./entities-providers/CRUDEntityProviderLocalDb";
import { CRUDEntityProviderWSO2EI, CRUDEntityProviderWSO2EIConfig } from "./entities-providers/CRUDEntityProviderWSO2EI";
import { CRUDEntityProviderQmatic, CRUDEntityProviderQmaticConfig } from "./entities-providers/CRUDEntityProviderQmatic";
import CRUDEntityProviderQmaticCita from "./entities-providers/CRUDEntityProviderQmaticCita";
import CRUDEntityProviderQmaticPerfilCita from "./entities-providers/CRUDEntityProviderQmaticPerfilCita";
import StoreGenericUseCaseListHelper from "./store/StoreGenericUseCaseListHelper";
import formatters from "./shared/formatters";
import utils from "./shared/utils";

export {
  AppConfiguration,
  AppError,
  AuthenticationUser,
  AuthenticationUserPermissions,
  AuthenticationUserProvider,
  AuthenticationUserProviderWSO2IS,
  CRUDEntity,
  CRUDEntityProvider,
  CRUDEntityProviderJSONFile,
  CRUDEntityProviderJSONFileConfig,
  CRUDEntityProviderLocalDb,
  CRUDEntityProviderLocalDbConfig,
  CRUDEntityProviderQmatic,
  CRUDEntityProviderQmaticCita,
  CRUDEntityProviderQmaticPerfilCita,
  CRUDEntityProviderQmaticConfig,
  CRUDEntityProviderWSO2EI,
  CRUDEntityProviderWSO2EIConfig,
  EntityList,
  EntityListParams,
  Filter,
  FilterFieldType,
  FilterOperator,
  formatters,
  StoreGenericUseCaseListHelper,
  utils
};

import AsFilter from "./components/AsFilter";
import AsFilterChipList from "./components/AsFilterChipList";
import AsFieldInput from "./components/AsFieldInput";
import AsFieldSelect from "./components/AsFieldSelect";
import AsDataItem from "./components/AsDataItem";
import AsDataList from "./components/AsDataList";
import AsDataListCardItem from "./components/AsDataListCardItem";
import AsFormGenerator from "./components/AsFormGenerator";
import AsSearchInput from "./components/AsSearchInput";
import AsFieldDate from "./components/AsFieldDate";
import AsFieldSelectDomain from "./components/AsFieldSelectDomain";
import WebDataRocks from "./components/WebDataRocks";
import { store } from "quasar/wrappers";

const components = [
  AsFilter,
  AsFilterChipList,
  AsFieldInput,
  AsFieldSelect,
  AsDataItem,
  AsDataList,
  AsDataListCardItem,
  AsFormGenerator,
  AsSearchInput,
  AsFieldDate,
  AsFieldSelectDomain,
  WebDataRocks
];

// Exportamos tb clases y tipos auxiliares para intercambiar info con los componentes

export default function install(Vue) {
  components.forEach(componentElement => {
    Vue.component(componentElement.name, componentElement);
  });
}
