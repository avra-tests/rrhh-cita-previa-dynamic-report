// import router from "./router";


const devWSO2Url = "http://ei.sandbox.dev.chakray.cloud";
// const devWSO2Url = "https://localhost:8080";

// object.freeze es para que no se pueda modificar por error las configuraciones
const configurationsByEnvironments = {
  localEnv: {
    authenticationProviderConfig: {
      ws02ServerUrl: "https://mgt-is.sandbox.dev.chakray.cloud",
      clientId: "wPp0J4aJGbhvmp3bM0BPCeibIz4a",
      clientSecret: "pTaOkMYN5L7jKSKuhn_vjN3ZsOUa",
      redirectUrl: "http://localhost:8080/"
    },
    permissionsRestProviderConfig: {
      restServerUrl: devWSO2Url,
      apiBasePath: "/seguridad/v1",
      resourceName: "obtenerPermisos",
      entityClassToProvide: null, // se asigna despues
      responseToEntityFieldsValuesFunction: null, // se asigna despues
      responseToEntityListFieldsValuesFunction: null // se asigna despues
    },
    empleadosRestProviderConfig: {
      restServerUrl: devWSO2Url,
      apiBasePath: "/directorio/v1",
      resourceName: "empleados",
      entityClassToProvide: null, // se asigna despues
      responseToEntityFieldsValuesFunction: null, // se asigna despues
      responseToEntityListFieldsValuesFunction: null, // se rellena en runtime
      useLocalDB: true
    }
  }
};

const responseToUserPermissionsFieldsValuesFunction = function(response) {
  return {
    usuario: response.data.datos.usuario,
    accesos: response.data.datos.accesos
  };
};

const chakrayResponseToEntityValuesFunction = function(response) {};

/**
 *
 * @param {*} response
 * @param {*} entityClassToProvide
 * @param {EntityListParams} listParams
 */
const chakrayResponseToEntityListValuesFunction = function(response, entityClassToProvide, listParams) {
  let responseListItems = [];

  try {
    // Esto es porque el response de chakray viene de la forma, p.e., response.data.datos.Empleado.Empleados
    responseListItems = utils.getFirstFieldValueOfObject(utils.getFirstFieldValueOfObject(response.data.datos));

    // esto es pq lo servicios entregados no lo devuelven nada por ahora
    if (!response.headers["X-Total-Count"]) {
      // TODO: cambiar cuando esté el servicio guay
      response.headers["X-Total-Count"] = responseListItems.length;
    }
  } catch (error) {
    throw new AppError(`Error conviertiendo response a entidad ${utils.getClassName(entityClassToProvide)}, excepcion: ${error}`);
  }

  /** @type {EntityList} */
  let list = new EntityList({
    items: responseListItems,
    itemsTotalCount: response.headers["X-Total-Count"],
    params: listParams
  });

  return list;
};

class AppConfiguration {
  _currentConfig;
  _routerInstance;
  _store;

  constructor(newAppConfig, router, store) {
    this._currentConfig = utils.cloneObjectValues(newAppConfig);
    this._routerInstance = router;
    this._store = store;
  }

  applyConfiguration() {
    this.setEntityProvidersImplementations(this._currentConfig);
    this.setRouterGuardForAuthenticatedRoutes(this._routerInstance, this._store);
  }

  setEntityProvidersImplementations(appConfig) {
    // Le decimos a las clases de logica de negocio que providers le darán la implementación para sus operaciones
    // dependientes de la infraestructura
    let authenticationUserProviderWSO2IS = new AuthenticationUserProviderWSO2IS(appConfig.authenticationProviderConfig);
    AuthenticationUser.provider = authenticationUserProviderWSO2IS;

    /**
     * @type {CRUDEntityProviderWSO2EIConfig}
     */
    let restproviderConfig;

    restproviderConfig = {
      ...appConfig.permissionsRestProviderConfig,
      authenticationUserProvider: authenticationUserProviderWSO2IS,
      authenticationUserPermissionsProvider: null, // no se necesitan permisos para pedir los permisos... ;)
      entityClassToProvide: AuthenticationUserPermissions,
      responseToEntityFieldsValuesFunction: responseToUserPermissionsFieldsValuesFunction
    };
    AuthenticationUserPermissions.provider = new CRUDEntityProviderWSO2EI(new CRUDEntityProviderWSO2EIConfig(restproviderConfig));

    restproviderConfig = {
      ...appConfig.empleadosRestProviderConfig,
      authenticationUserProvider: authenticationUserProviderWSO2IS,
      authenticationUserPermissionsProvider: AuthenticationUserPermissions.provider,
      entityClassToProvide: Empleado,
      responseToEntityFieldsValuesFunction: chakrayResponseToEntityValuesFunction,
      responseToEntityListFieldsValuesFunction: chakrayResponseToEntityListValuesFunction
    };

    let restProvider = new CRUDEntityProviderWSO2EI(new CRUDEntityProviderWSO2EIConfig(restproviderConfig));

    let localDbProviderConfig = new CRUDEntityProviderLocalDbConfig({
      entityClassToProvide: restproviderConfig.entityClassToProvide,
      entityIndices: [],
      getInitialCollectionFunction: async function() {
        let entityList = await restProvider.readList();
        return entityList.items;
      }
    });

    Empleado.provider = new CRUDEntityProviderLocalDb(localDbProviderConfig);
  }

  setRouterGuardForAuthenticatedRoutes(routerInstance, store) {
    // Configuramos la guarda de las rutas autenticadas
    routerInstance.beforeEach((to, from, next) => {
      try {
        store.dispatch("authentication/actionGetAuthenticatedUserOrLogin", {}, { root: true });

        next();
      } catch (error) {
        console.log(error);
      }
    });
  }
}

export { AppConfiguration, configurationsByEnvironments };

export default AppConfiguration;
