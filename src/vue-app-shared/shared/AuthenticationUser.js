import { AppError } from "../index.js";

/**
 * Interfaz del proveedor de la entidad
 */
class AuthenticationUserProvider {
  /**
   * @returns {Promise<boolean>} Devuelve true si ya estamos logado (para saber si hay que hace ro no login)
   */
  async isLogged() {
    throw new AppError("Provider no implementado");
  }

  /**
   * Dirige a la pantalla de login del proveedor OAuth
   */
  loginWithRedirect() {
    throw new AppError("Provider no implementado");
  }

  /**
   * @returns {Promise<AuthenticationUser>}
   */
  async getAuthenticatedUser() {
    throw new AppError("Provider no implementado");
  }
}

class AuthenticationUser {
  /**
   * La clase proveedor que va a proporcionar estas entidades
   *
   * @private
   * @type {AuthenticationUserProvider}
   */
  static _entityProvider;
  id;
  nombre;
  apellidos;
  dni;
  photoUrl;

  static set provider(newEntityProvider) {
    this._entityProvider = newEntityProvider;
  }

  /**
   * @returns {AuthenticationUserProvider}
   * (todo: esto habría que cambiarlo por una interfaz)
   */
  static get provider() {
    return this._entityProvider;
  }

  constructor({ id, nombre, apellidos, photoUrl, dni }) {
    this.id = id;
    this.nombre = nombre;
    this.apellidos = apellidos;
    this.dni = dni;
    this.photoUrl = photoUrl;
  }
}

export { AuthenticationUser, AuthenticationUserProvider };
export default AuthenticationUser;
