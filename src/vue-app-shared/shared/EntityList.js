import { utils } from "../index.js";

class EntityListParams {
  /**
   * Filtro en formato mango (mongodb)
   * @type {Object}
   */
  filter;

  /** @type {number} */
  paginationStart;

  /** @type {number} */
  paginationLimit;

  /**
   * @typedef {Object} EntityListSort
   * @property {string} fieldName
   * @property {boolean} isDescending
   */
  /** @type {EntityListSort} */
  sort;

  /**
   * @param {object=} fieldValues
   */
  constructor({ paginationStart, paginationLimit, sort, filter }) {
    utils.copyValuesInClassObjectProperties({ paginationStart, paginationLimit, sort, filter }, this);
  }
}

class EntityList {
  /** @type {Array<Object>} */
  items;

  /**
   * @type {number}
   * Si el listado es paginado el total de elementos que cumplen los criterios de filtrado (no solo los paginados)
   * */
  itemsTotalCount;

  /** @type {EntityListParams} */
  params;

  /**
   * Devuelve un objeto mapa cuyos atributos son los id's. Es la estructura más rápida para consultar por id si está todo cargado
   * @returns {Object}
   */
  convertToMapById(key) {
    return utils.convertArrayToObject(this.items, key);
  }

  /**
   * Devuelve un array de opciones para un select basado en el listado y poniendo en label el campos pasados por parametro
   * y como value el campo "id"
   * @param {string} labelProperty
   * @returns {Array<{ labelProperty: string, valueProperty: string }>}
   */
  convertToSelectOptions(labelProperty, valueProperty) {
    return utils.convertObjectArrayToSortedSelectOptions({
      array: this.items,
      labelProperty,
      valueProperty
    });
  }

  /**
   * Devuelve un array de opciones para un select basado en el listado y poniendo en label el campos pasados por parametro
   * y como value el campo que viene como id
   * @param {string} labelProperty
   * @param {string} valueProperty 
   * @param {string} valueParentProperty
   * @returns {Array<{ labelProperty: string, valueProperty: string }>}
   */
  convertToSelectOptionsWithParent(labelProperty, valueProperty, valueParentProperty) {
    return utils.convertObjectArrayToSortedSelectOptionsWithParent({
      array: this.items,
      labelProperty,
      valueProperty: valueProperty,
      valueParentProperty
    });
  }

  /**
   * Devuelve un array de opciones para un select basado en el listado y poniendo en label el campos pasados por parametro
   * y como value el campo que viene como id
   * @param {string} labelProperty
   * @param {string} valueProperty 
   * @param {string} valueDomainProperty
   * @returns {Array<{ labelProperty: string, valueProperty: string }>}
   */
  convertToSelectOptionsWithDomain(labelProperty, valueProperty, valueDomainProperty) {
    return utils.convertObjectArrayToSortedSelectOptionsWithDomain({
      array: this.items,
      labelProperty,
      valueProperty: valueProperty,
      valueDomainProperty
    });
  }
  /**
   *
   */
  constructor({ items, itemsTotalCount, params }) {
    let defaultListParams = new EntityListParams({
      filter: null,
      paginationLimit: null,
      paginationStart: null,
      sort: null
    });

    defaultListParams.sort;

    // items los asignamos directamente pq esturcutras completas no son para copiar sus valores, 
    // no queremos perder los objetos (metodos, getters...)
    this.items = items;

    utils.copyValuesInClassObjectProperties({ itemsTotalCount, params: params ? params : defaultListParams }, this);
  }
}

export { EntityList, EntityListParams };
export default EntityList;
