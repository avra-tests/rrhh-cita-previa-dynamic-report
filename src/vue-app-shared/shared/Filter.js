import { utils } from "../index.js";
import AppError from "./AppError.js";

/**
 * Tipos de fields
 * @enum {string}
 */
const FilterFieldType = Object.freeze({
  SELECT: "select",
  INPUT: "input"
});

/**
 * Tipos de fields
 * operador como el de mongobd https://docs.mongodb.com/manual/reference/operator/query/
 * @enum {string}
 */
const FilterOperator = {
  EQUAL: "$eq",
  LIKE: "$regexp"
};

/**
 * @typedef {Object} FilterField
 * @property {string} name
 * @property {string} value
 * @property {FilterOperator} operator
 * @property {object} regex regEx para aplicar al value
 * @property {string} regex.pattern cadena del patrón, se sustituye ${value} por el valor del value
 * @property {string} regex.options options de la expresion regular
 * @property {FilterFieldType} type
 * @property {object} extra Extra info fields que se necesite p.e. para aprovechar para campos que se necesiten en la ui o el provider...
 */

class Filter {
  /**
   * @type {Array<FilterField>}
   */
  filterFields;

  /**
   * @type {string}
   */
  fullTextSearch;

  /**
   * @returns {object} Deuelve el filtro de la forma objeto {nombre:{}, apellidos1: {}}
   */
  toObjectWithAllFields() {
    let filterObject = {};

    this.filterFields.forEach(({ name, value, operator, type }) => {
      filterObject[name] = { name, value, operator, type };
    });

    return filterObject;
  }

  /**
   * @return {Array<FilterField>}
   */

  getFilterFieldsNotEmpty() {
    let filterFieldsNotEmpty = this.filterFields.filter(field => {
      return !(field && (field.value === "" || field.value === null || field.value === undefined));
    });
    return utils.cloneObjectValues(filterFieldsNotEmpty);
  }

  resetFilterFieldsValues() {
    this.filterFields.forEach(field => {
      field.value = "";
    });
  }

  /**
   * @private
   * @return {object} Query en formato mongodb https://docs.mongodb.com/manual/reference/operator/query/
   */
  _getMongoDbQueryWithAndOperator() {
    let mongodbQueryTerms = [];

    this.getFilterFieldsNotEmpty().forEach(field => {
      // mongodb term sample {field: {$gt: value} }
      let queryTermValue = {};

      if (field.operator === "$regex") {
        let regexValue = field.regex ?? { pattern: "${value}", options: "i" };

        regexValue.pattern = regexValue.pattern.replace("${value}", field.value);

        queryTermValue = { $regex: regexValue.pattern, $options: regexValue.options };
      } else {
        queryTermValue[field.operator] = field.value;
      }

      let newTerm = {};
      newTerm[field.name] = queryTermValue;
      mongodbQueryTerms.push(newTerm);
    });
    return { $and: mongodbQueryTerms };
  }

  /**
   * @returns {object} Deuelve el filtro de la forma objeto {nombre:{}, apellidos1: {}}
   */
  toObjectWithNotEmptyFields() {
    let filterObject = {};

    this.getFilterFieldsNotEmpty().forEach(({ name, value, operator, type }) => {
      if (!(value === "" || value === null || value === undefined)) {
        filterObject[name] = { name, value, operator, type };
      }
    });

    return filterObject;
  }

  /**
   * @returns {object} Objeto query en fomrato mongodb dependiendo de los valores del filtro
   */
  getFilterQuery() {
    return this._getMongoDbQueryWithAndOperator();
  }

  /**
   * @param {object} params
   * @property {Array<FilterField>} params.filterFields
   * @property {string=} params.fullTextSearch
   */
  constructor({ filterFields, fullTextSearch }) {
    utils.copyValuesInClassObjectProperties({ filterFields, fullTextSearch }, this);
  }
}

export { Filter, FilterFieldType, FilterOperator };
