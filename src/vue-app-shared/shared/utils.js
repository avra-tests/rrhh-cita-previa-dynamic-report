import AppError from "../shared/AppError";
import { runInNewContext } from "vm";
import { CRUDEntity } from "./CRUDEntity";

/**
 * Metodos de utilidad comunes a todo código de aplicación, para evitar reescribir código
 */
const utils = {
  /**
   * Clona un objeto pasado, devolviendo un objeto distinto al pasado pero con los mismos valores
   * Clona toda la estructura.
   * @param {object} object Objeto que se quiere clonar
   * @throws SyntaxError si el objeto no es correcto
   */
  cloneObjectValues(object) {
    var clonedObject = null;

    clonedObject = JSON.parse(JSON.stringify(object));

    return clonedObject;
  },
  /**
   * Clona un objeto pasado con solo las propiedades que cumplan la funcion pasada
   * @param {objectToFilter} object Objeto que se quiere clonar
   * @param {Function} objectEntryFilterFunction Objeto que se quiere clonar que toma dos parametros propertyName, propertyValue
   * @throws SyntaxError si el objeto no es correcto
   */
  cloneObjectValuesWithFilteredProperties(objectToFilter, objectEntryFilterFunction) {
    var clonedObject = null;

    let objectToFilterEntries = Object.entries(objectToFilter);
    let clonedObjectEntries = objectToFilterEntries.filter(([propertyName, propertyValue]) =>
      objectEntryFilterFunction(propertyName, propertyValue)
    );
    clonedObject = Object.fromEntries(clonedObjectEntries);

    return this.cloneObjectValues(clonedObject);
  },
  compareStrings(str1, str2) {
    return (str1 ?? "").localeCompare(str2 ?? "");
  },
  /**
   * Copia los campos de un objeto en las propiedades de la clase del objeto destino
   * p.e. para inicializar objetos de clases con valores pasados
   * @param {object} fieldsValuesObject
   * @param {object} destinationObject
   */
  copyValuesInClassObjectProperties(fieldsValuesObject, destinationObject) {
    if (!this.isObjectOfDefinedClass(destinationObject)) {
      throw new AppError(
        `copyValuesInClassObjectProperties: se esperaba un objeto instancia de una clase definida y se ha recibido: ${destinationObject}`
      );
    }

    let entriesThatPropertyExistsInDestinationObject = Object.entries(fieldsValuesObject).filter(([key, value]) =>
      destinationObject.hasOwnProperty(key)
    );

    entriesThatPropertyExistsInDestinationObject.forEach(([key, value]) => {
      if (value === null || value === undefined) {
        destinationObject[key] = value;
      } else if (value instanceof Function) {
        // Clase o funcion, se copia la referencia
        destinationObject[key] = value;
      } else if (value instanceof Array) {
        destinationObject[key] = this.cloneObjectValues(value);
      } else if (this.isObjectOfDefinedClass(value)) {
        // Es una refrencia de una clase, copiamos la referencia (p.e. authenticacionProvider)
        destinationObject[key] = value;
      } else {
        // Un objetio simple o valor a copiar
        destinationObject[key] = this.cloneObjectValues(value);
      }
    });
  },
  /**
   *
   * @param {Array<object>} array Array de objetos
   * @param {string} key campo de los elementos del array que se usará como campos del objeto resultante
   */
  convertArrayToObject(array, key) {
    if (!Array.isArray(array)) {
      throw new AppError(`convertArrayToObject: se esperaba un array y se recibió:${JSON.stringify(array)}`);
    }
    const initialValue = {};
    return array.reduce((obj, item) => {
      return {
        ...obj,
        [item[key]]: item
      };
    }, initialValue);
  },
  /**
   *
   * @param {*} params
   * @param {Array} params.array
   * @param {string} params.labelProperty
   * @param {string} params.valueProperty
   */
  convertObjectArrayToSortedSelectOptions({ array, labelProperty, valueProperty }) {
    if (!Array.isArray(array)) {
      throw new AppError(`convertObjectArrayToSelectOptions: se esperaba un array y se recibió:${JSON.stringify(array)}`);
    }

    return array
      .map(item => ({ label: item[labelProperty], value: item[valueProperty] }))
      .sort((a, b) => ("" + a.label).localeCompare(b.label));
  },
  /**
   *
   * @param {*} params
   * @param {Array} params.array
   * @param {string} params.labelProperty
   * @param {string} params.valueProperty
   * @param {string} params.valueParentProperty
   */
  convertObjectArrayToSortedSelectOptionsWithParent({ array, labelProperty, valueProperty, valueParentProperty }) {
    if (!Array.isArray(array)) {
      throw new AppError(`convertObjectArrayToSelectOptions: se esperaba un array y se recibió:${JSON.stringify(array)}`);
    }

    return array
      .map(item => ({ label: item[labelProperty], value: item[valueProperty], parentValue: item[valueParentProperty] }))
      .sort((a, b) => ("" + a.label).localeCompare(b.label));
  },
  /**
   *
   * @param {*} params
   * @param {Array} params.array
   * @param {string} params.labelProperty
   * @param {string} params.valueProperty
   * @param {string} params.valueDomainProperty
   */
  convertObjectArrayToSortedSelectOptionsWithDomain({ array, labelProperty, valueProperty, valueDomainProperty }) {
    if (!Array.isArray(array)) {
      throw new AppError(`convertObjectArrayToSelectOptions: se esperaba un array y se recibió:${JSON.stringify(array)}`);
    }

    return array
      .map(item => ({ label: item[labelProperty], value: item[valueProperty], domain: item[valueDomainProperty] }))
      .sort((a, b) => ("" + a.label).localeCompare(b.label));
  },
  /**
   * @param {Date} startDate
   * @param {Date} endDate
   * @returns {Array<Date>}
   */
  createDaysBetweenDates(startDate, endDate) {
    let dates = [];
    let currentDate = new Date(startDate.toDateString());

    while (currentDate <= endDate) {
      dates.push(currentDate);
      currentDate = this.dateWithDaysAdded(currentDate, 1);
      // console.log("🚀 ~ createDaysBetweenDates ~ currentDate", currentDate);
    }
    return dates;
  },
  /**
   * @param {Date} date
   * @param {number} daysToAdd
   * @returns {Date}
   */
  dateWithDaysAdded(date, daysToAdd) {
    return new Date(new Date(date).setDate(date.getDate() + daysToAdd));
  },
  /**
   *
   * @param {Date} date
   * @param {string} hourMinutesString
   * @returns void
   */
  getDateSettingHourMinutes(date, hourMinutesString) {
    let [hour, minutes, seconds] = [...hourMinutesString.split(":")];
    let newDate = new Date(date.getTime());

    newDate.setHours(Number.parseInt(hour));
    newDate.setMinutes(Number.parseInt(minutes));

    return newDate;
  },

  /**
   *
   * @param {Date} date
   */
  getYYMMDDDateString(date) {
    return `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, "0")}-${date
      .getDate()
      .toString()
      .padStart(2, "0")}`;
  },

  /**
   *
   * @param {Date} date
   */
  getHHMMTimeString(date) {
    return `${date
      .getHours()
      .toString()
      .padStart(2, "0")}:${date
      .getMinutes()
      .toString()
      .padStart(2, "0")}`;
  },

  /**
   * @param {Date} date
   * @retuns {Number}
   */
  getWeekDayNumber(date) {
    // No lo haog con date.getDay() porque depende del locale del navegador y en ingles no es igual que en Españórrr
    let weekdays = {
      lunes: 0,
      martes: 1,
      miércoles: 2,
      miercoles: 2,
      jueves: 3,
      viernes: 4,
      sábado: 5,
      sabado: 5,
      domingo: 6
    };

    return weekdays[date.toLocaleDateString("es-ES", { weekday: "long" }).toLowerCase()];
  },

  /**
   *
   * @param {*} previousTimeToCita
   * @param {*} currentCita
   * @param {*} fecha
   *
   * @returns {number}
   */
  getMinutesBetweenDates(date2, date1) {
    return Math.trunc((date2 - date1) / 60000);
  },

  /**
   *
   * @param {object} fieldsValuesObject
   * @param {FunctionConstructor} EntityClass
   */
  entityObjectFromFieldsValuesObject(fieldsValuesObject, EntityClass) {
    /**@type {CRUDEntity} */
    let newEntity = new EntityClass(fieldsValuesObject);

    return newEntity;
  },
  /**
   * Devuelve la url hasta que empieza las rutas, o sea, hasta el /#/
   * @returns {string} url base para sin la ruta
   */
  getCurrentBaseUrlWithoutRoute(currentWindowLocation) {
    var baseUrl = currentWindowLocation.href.replace(/[a-z]*\.html/i, "").replace(currentWindowLocation.hash ? "/" + currentWindowLocation.hash : "", "");

    if (baseUrl[baseUrl.length - 1] === "/") {
      baseUrl = baseUrl.substr(0, baseUrl.length - 1);
    }
    return baseUrl;
  },
  /**
   * Devuelve el valor del primer campo del objeto, útil para cunado vienen json del tipo datos.Empleado.Empleados..
   * @param {*} obj
   * @returns any
   */
  getFirstFieldValueOfObject(obj) {
    return obj[Object.keys(obj)[0]];
  },
  /**
   * Devuelve el nombre de la clase que se le pasa. Ej: Empleados
   * @param {Function} entityClass
   * @returns {string}
   */
  // Se quita pq en produccion minificado no sirve para nada
  // getClassName(entityClass) {
  //   return entityClass.prototype.constructor.name;
  // },
  /**
   * Sobre el objeto pasado, obtiene y devuelve el valor de la propiedad pasada por cadena (puede ser en varios niveles separadas por
   * puntos como p.e. "centro.provincia.nombre") o el valor indicado en nullValue (o null si no se especifica) si no existe algunas
   * de las propiedades
   * @param {object} object
   * @param {string} dotStringFields cadena como se accedería a las propierades del objecto p.e. "centro.provincia.nombre"
   * @return {any} devuelve el valor de la propiedad o nulo si no existe algunas de las propiedades
   */
  getPropertyNVL(object, dotStringFields, nullValue) {
    return dotStringFields.split(".").reduce((object, propertyName) => {
      if (
        object instanceof Object &&
        object.hasOwnProperty(propertyName) &&
        object[propertyName] !== null &&
        object[propertyName] !== nullValue
      ) {
        return object[propertyName];
      } else {
        return nullValue;
      }
    }, object);
  },
  /**
   * Devuelve una nueva url pero cambiando el recurso, útil para cuando se quiere hacer un link a un recurso en la misma página
   * @param {string} oldUrl
   * @param {string} newResource
   */
  getUriWithSamePathReplacingResource(oldUrl, newResource) {
    // Sustituye lo ultimo '/<resource>.xxxx' por el nuevo recurso
    return oldUrl.replace(/\/(?=[^/]*$).+\..+/, `/${newResource}`);
  },
  /**
   * Devuelve igual si la estructura y los valores de los objetos pasados son iguales
   * @param {object} object1
   * @param {object} object2
   */
  hasEqualsValues(object1, object2) {
    return JSON.stringify(object1) === JSON.stringify(object2);
  },
  /**
   * @param {object} objectToCheck
   * @returns {boolean} Verdadero si el objeto pasado es una instancia de una clase definida, p.e.
   */
  isObjectOfDefinedClass(objectToCheck) {
    return objectToCheck && objectToCheck.constructor && objectToCheck.constructor && objectToCheck.constructor.name !== "Object";
  },
  isToday(someDate) {
    const today = new Date()
    return someDate.getDate() == today.getDate() &&
      someDate.getMonth() == today.getMonth() &&
      someDate.getFullYear() == today.getFullYear()
  },
  redirectBrowserToPage(newPageUrl) {
    window.location.href = newPageUrl;
  },
  /**
   *
   * @param {*} string cadena regex con opciones como /hue/i
   * @returns {Array|string} Value para pasarselo al constructor de RegExp (cadena o array dependiendo si tiene opciones)
   */
  regexValueFromString(string) {
    var match = /^\/(.*)\/([a-z]*)$/.exec(string);
    if (match[2]) {
      return [match[1], match[2]];
    } else {
      return match[1];
    }
  },
  /**
   * Elimina las tildes de una cadena y las sustituye por letras sin tildes
   * @param {string} stringToConvert
   * @returns {string}
   */
  replaceDiacritics(stringToConvert) {
    return stringToConvert.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  },
  /**
   * Devuelve una promesa que espera el numero de milisegundos pasados
   * @param {*} millisecons
   */
  sleep(millisecons) {
    return new Promise(resolve => setTimeout(resolve, millisecons));
  },

  /**
   * Espera o duerme mientras no se cumpla la condición de salida pasado como función
   *
   * @param {number} millisecons
   * @param {Function} isTimeContinueSleepingFunction funcion que devuelve si se sigue durmiendo
   * @param {number} timeout Timeout de seguridad para que no se quede "forever and never"
   */
  async sleepWhile(isTimeContinueSleepingFunction, millisecons = 100, timeout = 5000) {
    let timeSeeping = 0;
    while (isTimeContinueSleepingFunction()) {
      await this.sleep(millisecons);
      timeSeeping += millisecons;
      if (timeSeeping > timeout) {
        throw new AppError(`Se ha superado el timeout definido en sleepWhile: ${timeout}`);
      }
    }
  }
};

export default utils;
