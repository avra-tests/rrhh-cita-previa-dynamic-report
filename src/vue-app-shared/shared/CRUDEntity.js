import AppError from "../shared/AppError";
import { EntityList, EntityListParams } from "../shared/EntityList";
import utils from "../shared/utils";

/**
 * Interfaz del proveedor de la entidad
 */
class CRUDEntityProvider {
  /**
   * Devuelve el elementos del recurso (o entidad), cuando no se necesita buscar por id, pq solo hay un elemento de ese recurso,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   *
   * @returns {Promise<Object>}
   */
  async readItem() {
    throw new AppError("Función de provider de entidad no implementado");
  }

  /**
   * Devuelve un un objeto EntityList con el listado (e información de filtro, paginacion...) de elementos del recurso (o entidad)
   * que cumplen ciertos criterios
   * @param {EntityListParams} listParams
   * @returns {Promise<EntityList>}
   */
  async readList(listParams) {
    throw new AppError("Función de provider de entidad no implementado");
  }

  /**
   * Crea un nuevo regisgtro con los datos y
   * Devuelve el elementos del recurso (o entidad) actualizado,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   * @param {CRUDEntity=} item objeto que se desea actualizar
   * @returns {Promise<Object>}
   */
  async createItem(item) {
    throw new AppError("Función de provider de entidad no implementado");
  }

  /**
   * Actualiza la totalidad de los datos y
   * Devuelve el elementos del recurso (o entidad) actualizado,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   * @param {CRUDEntity=} item objeto que se desea actualizar
   * @returns {Promise<Object>}
   */
  async updateItem(item) {
    throw new AppError("Función de provider de entidad no implementado");
  }

  /**
   * Actualiza solo los campos que se reciben en el objeto correspondiente
   * Devuelve el elementos del recurso (o entidad) actualizado,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   * @param {Object=} objetct objeto Json que se desea actualizar.
   * @returns {Promise<Object>}
   */
  async patchItem(object) {
    throw new AppError("Función de provider de entidad no implementado");
  }
}

/**
 * Clase base para todas las entidades, que básicamente se encargar del código común de las entidades de la lógica de negocio,
 * p.e, establecer el provider y ofrecer el provider de la entidad, que además en este caso, será siempre la misma interfaz del
 * provider, para simplificar el código y la implementación del provider
 */
class CRUDEntity {
  /**
   * La clase proveedor que va a proporcionar estas entidades
   *
   * @private
   * @type {CRUDEntityProvider}
   */
  static _entityProvider;

  /**
   * Id unico
   * @type {string}
   */
  id;

  /**
   * Funcion a ejecutar (en caso de estar definida) tras la carga de datos para cargar info sobre entidades relacionadas...
   * @type {Function}
   * @returns {void}
   */
  readRelatedEntitiesProperties;

  static set provider(newEntityProvider) {
    this._entityProvider = newEntityProvider;
  }

  /**
   * @returns {CRUDEntityProvider}
   */
  static get provider() {
    return this._entityProvider;
  }

  /**
   * Devuelve el elementos del recurso (o entidad), cuando no se necesita buscar por id, pq solo hay un elemento de ese recurso,
   * p.e. el objeto AuthenticationUserPermissions que solo hay posibilidad de uno
   *
   * @returns {Promise<Object>}
   */
  readItem() {
    throw new AppError("Función de provider de entidad no implementado");
  }

  /**
   * @protected
   */
  _setEntityFieldsValues(fieldsValuesObject) {
    utils.copyValuesInClassObjectProperties(fieldsValuesObject, this);
  }
}

export { CRUDEntity, CRUDEntityProvider };
