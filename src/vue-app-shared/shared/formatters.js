import AppError from "../shared/AppError";
import { format } from "quasar";
import { date } from "quasar"
const { capitalize } = format;

/**
 * Metodos de utilidad para formatear valores
 */
const formatters = {

  /**
   * Formatea a las letras de cada palabla de la cadena a mayúsculas y el resto lo deja en minúsculas
   * @param {string} valueToFormat 
   * @returns {string}
   */
  capitalize(valueToFormat) {
    if (valueToFormat && typeof valueToFormat === "string") {
      return capitalize(valueToFormat.toLowerCase());
    } else {
      return "";
    }
  },
   /**
   * Formatea a las letras de cada palabla de la cadena a mayúsculas y el resto lo deja en minúsculas
   * @param {date} valueToFormat 
   * @returns {string}
   */
  formatDate(valueToFormat) {
    if (valueToFormat) {
      return date.formatDate(valueToFormat,'DD/MM/YYYY');
    } else {
      return "";
    }
  },
  formatDateBD(valueToFormat) {    
    if (valueToFormat){
      valueToFormat = valueToFormat.split('/').reverse().join('-')
      if(date.isValid(valueToFormat)) {
        return valueToFormat;
      } else {
        return "";
      }
    } else {        
      return "";
    }
  }
};

export default formatters;
