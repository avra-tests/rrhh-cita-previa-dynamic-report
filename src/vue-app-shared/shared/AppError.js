import AppConfiguration from "./AppConfiguration"

/**
 * Clase ampliable para lanzar erorres y detectarlos en los actions
 */
class AppError extends Error {
  static handleError(error, customMsg) {
    console.log("Error:", { customMsg, error });
    console.log("AppConfiguration._store", AppConfiguration);
  }
}

export default AppError;