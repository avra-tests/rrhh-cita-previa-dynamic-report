import {
  AppError,
  CRUDEntity,
  CRUDEntityProvider,
  CRUDEntityProviderLocalDb,
  CRUDEntityProviderLocalDbConfig,
  CRUDEntityProviderJSONFile,
  CRUDEntityProviderJSONFileConfig,
  EntityList,
  EntityListParams,
  utils
} from "src/vue-app-shared";

/**
 * @typedef {object} EntityProviderConfig
 * @property {object} entityClassNameToProvide Clase de la entidad, p.e. Empleado*
 * @property {object} entityClassToProvide Clase de la entidad, p.e. Empleado
 * @property {object} providerConfigClass Clase de configuración del proveedor de la entidad (para crear la conf al provider)
 * @property {object} providerClass Clase proveedor de la entidad
 * @property {string} restServerUrl
 * @property {string} apiBasePath
 * @property {string} resourceName
 * @property {boolean} useLocalCachedDB
 */

/**
 * @typedef {object} AppConfig
 * @property {Array<EntityProviderConfig>} entityConfigs
 */

class AppConfiguration {
  /**
   * @type {AppConfig}
   */
  static _currentConfig;
  _routerInstance;
  static _store;

  constructor(newAppConfig, router, store) {
    this._currentConfig = newAppConfig;
    this._routerInstance = router;
    this._store = store;    
  }

  applyConfiguration() {
    this.setEntityProvidersImplementations(this._currentConfig);
    // this.setRouterGuardForAuthenticatedRoutes(this._routerInstance, this._store);
  }

  /**
   *
   * @param {AppConfig} appConfig
   */
  setEntityProvidersImplementations(appConfig) {
    if (!appConfig.entityConfigs) {
      throw new AppError("AppConfiguration: se esperaba un array con los providers de las entidades");
    }

    console.log("AppConfiguration: setEntityProvidersImplementations", { appConfig });
    appConfig.entityConfigs.forEach(entityConfig => {
      let providerClass = entityConfig.providerClass;
      let configProviderClass = entityConfig.providerConfigClass;
      let entityClass = entityConfig.entityClassToProvide;
      // Se tiene que guardar el nombre "a jierro" pq si no es así, en la minimificación se pierde el nombre original de la clase
      let entityClassName = entityConfig.entityClassNameToProvide;

      let entityProvider = new providerClass(new configProviderClass(entityConfig));
      entityProvider.entityClassToProvide = entityClass;

      if (entityConfig.useLocalCachedDB) {
        let localDbProviderConfig = new CRUDEntityProviderLocalDbConfig({
          entityClassNameToProvide: entityClassName,
          entityClassToProvide: entityProvider.entityClassToProvide,
          entityIndices: ["id"],
          getInitialCollectionFunction: async function() {
            let entityList = await entityProvider.readList();

            for (let entityObject of entityList.items) {
              // Si tiene definida funcion para cargar atributos de entidades relacionadas lo cargamos
              if (typeof entityObject.readRelatedEntitiesProperties === "function") {
                await entityObject.readRelatedEntitiesProperties();
              }
            }

            return entityList.items;
          }
        });

        entityClass.provider = new CRUDEntityProviderLocalDb(localDbProviderConfig);
      } else {
        entityClass.provider = entityProvider;
      }
    });
  }

  setRouterGuardForAuthenticatedRoutes(routerInstance, store) {
    // Configuramos la guarda de las rutas autenticadas
    routerInstance.beforeEach((to, from, next) => {
      try {
        store.dispatch("authentication/actionGetAuthenticatedUserOrLogin", {}, { root: true });

        next();
      } catch (error) {
        console.log(error);
      }
    });
  }
}

export default AppConfiguration;
