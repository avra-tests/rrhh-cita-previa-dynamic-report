import { CRUDEntity, CRUDEntityProvider } from "../shared/CRUDEntity";

class AuthenticationUserPermissions extends CRUDEntity {
  /**
   * @typedef {Object} PermissionsUserInfo
   * @property {string} txtLogin
   * @property {string} txtDni
   * @property {string} txtEmail
   * @property {string} jsonUsuAmbito
   * @property {string} idUsuario
   * @property {string} idFuncion
   */
  /** @type {PermissionsUserInfo} */
  usuario;

  /**
   * @typedef PermissionsAccesos
   *
   * @property {string} funcion
   * @property {string} idNivelAmbito
   * @property {string} lgEsPorDefecto
   * @property {string} txtComentario
   * @property {string} jsonFuncAmbito
   * @property {array} accesosApis
   */
  /** @type {PermissionsAccesos} */
  accesos;

  constructor(fieldValuesObject) {
    super();
    this._setEntityFieldsValues(fieldValuesObject);
  }
}

export default AuthenticationUserPermissions;
