import { AppError } from "src/vue-app-shared";

/**
 * Clase para facilitar la creación de actiones repetitivas (y sus mutaciones de inicio y fin) de casos de uso de listado
 * cuyo state es un list y un isLoading, se "autogeneran" la smutacion en funcion del nombre del caso de uso
 */
export default class StoreGenericUseCaseListHelper {
  /** @type {String} */ mutationEndWithErrorName;
  /** @type {String} */ stateLoadingParamName;
  /** @type {String} */ stateListParamName;
  /** @type {String} */ mutationBeginName;
  /** @type {String} */ mutationEndName;
  /** @type {String} */ useCaseName;

  checkRequiredPayloadParam(payload, paramName) {
    if (!payload && !payload[paramName]) {
      throw new AppError(
        `action de case "${this.useCaseName}": Se esperaba parámetro "${paramName}" en el payload y no se ha recibido nada`
      );
    }
  }

  /**
   * Ejecuta la acción generica de llamar a la funcion async "entityReadListFunction" pasada
   * @param { {context: String, payload: Object, listParamsName: String, entityReadListFunction: Function} } actionParam
   */
  async actionViewEntityList({ context, payload, listParamsName, entityReadListFunction }) {
    try {
      this.checkRequiredPayloadParam(payload, listParamsName);

      context.commit(this.mutationBeginName);
      let list = await entityReadListFunction();
      context.commit(this.mutationEndName, { list });
    } catch (error) {
      AppError.handleError(error);
      context.commit(this.mutationEndWithErrorName);
      throw error;
    }
  }

  /** @private */
  _mutationViewEntityListBegin(state) {
    state[this.stateLoadingParamName] = true;
  }

  /** @private */
  _mutationViewEntityListEndWithError(state) {
    state[this.stateLoadingParamName] = false;
  }

  /** @private */
  _mutationViewEntityListEnd(state, { list }) {
    state[this.stateListParamName] = list;
    state[this.stateLoadingParamName] = false;
  }

  /**
   * Devuelve un objecto de mutaciones para pasarselas directamente a las mutaciones del store basado en el casename
   * algo como { <casename>Begin, <casename>End, <casename>EndwithError }
   */
  getMutations() {
    let mutations = {};

    mutations[this.mutationBeginName] = state => this._mutationViewEntityListBegin(state);
    mutations[this.mutationEndName] = (state, { list }) => this._mutationViewEntityListEnd(state, { list });
    mutations[this.mutationEndWithErrorName] = state => this._mutationViewEntityListEndWithError(state);
    return mutations;
  }

  /**
   *
   * @param {*} params
   * @param {String} params.useCaseName Nombre del caso de uso, se usa para autogenerar nombres de mutaciones
   * @param {String} params.stateLoadingParamName Nombre de la variable del state
   * @param {String} params.stateListParamName
   */
  constructor({ useCaseName, stateLoadingParamName, stateListParamName }) {
    this.useCaseName = useCaseName;
    this.stateLoadingParamName = stateLoadingParamName;
    this.stateListParamName = stateListParamName;
    this.mutationBeginName = `${this.useCaseName}Begin`;
    this.mutationEndName = `${this.useCaseName}End`;
    this.mutationEndWithErrorName = `${this.useCaseName}EndWithError`;
  }
}
