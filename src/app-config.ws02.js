import {
  CRUDEntityProviderJSONFile,
  CRUDEntityProviderJSONFileConfig,
  utils,
  CRUDEntityProviderWSO2EI,
  CRUDEntityProviderWSO2EIConfig
} from "src/vue-app-shared";

import Provincia from "src/store/shared/Provincia";
import Puesto from "src/store/shared/Puesto";
import Centro from "src/store/shared/Centro";
import Dominio from "src/store/shared/Dominio";

import Finalidad from "src/store/finalidad/Finalidad";
import TipoFuente from "src/store/tipoFuente/TipoFuente";
import PlanFinancion from "src/store/planFinanciacion/PlanFinanciacion";
import Hito from "src/store/hito/Hito";
import Actuacion from "src/store/actuacion/Actuacion";
import ProyectoFeder from "./store/proyectoFeder/ProyectoFeder";
import ProgramaFinanciacion from "./store/programaFinanciacion/ProgramaFinanciacion";

import Empleado from "src/store/empleados/Empleado";
import Favorito from "src/store/empleados/Favorito";


const sameAppServerUrl = `${utils.getCurrentBaseUrlWithoutRoute(window.location)}`;
const publicFolderWithJsonFiles = "/static-rest";

const empleadoClass = Empleado;

const esbURL = "http://localhost:8081";
const esbApiName = "/apiFF/financia";

let appConfig = {
  menuItems: [
    { label: "Inicio", icon: "home", routePath: "/" },
    { label: "Empleados", icon: "group", routePath: "/empleados" },
    { label: "Finalidades", icon: "assignment_ind", routePath: "/finalidades" },
    { label: "Tipos de Fuente de Financiación", icon: "monetization_on", routePath: "/tipoFuente" },
    { label: "Definición de Hitos", icon: "event", routePath: "/hitos" },
    { label: "Plan de Financiación", icon: "assessment", routePath: "/planFinanciacion" },
    { label: "Proyectos Feder", icon: "public", routePath: "/proyectosFeder" },
    { label: "Programas de Financiación", icon: "local_atm", routePath: "/programasFinanciacion" },
    { label: "Busqueda de Actuaciones", icon: "handyman", routePath: "/actuacion" },    
    
  ],
  entityConfigs: [
    {
      entityClassNameToProvide: "Empleado",
      entityClassToProvide: Empleado,
      providerConfigClass: CRUDEntityProviderJSONFileConfig,
      providerClass: CRUDEntityProviderJSONFile,
      restServerUrl: sameAppServerUrl,
      apiBasePath: publicFolderWithJsonFiles,
      resourceName: "empleados.json",
      useLocalCachedDB: true
    },
    {
      entityClassNameToProvide: "Provincia",
      entityClassToProvide: Provincia,
      providerConfigClass: CRUDEntityProviderJSONFileConfig,
      providerClass: CRUDEntityProviderJSONFile,
      restServerUrl: sameAppServerUrl,
      apiBasePath: publicFolderWithJsonFiles,
      resourceName: "provincias.json",
      useLocalCachedDB: true
    },
    {
      entityClassNameToProvide: "Puesto",
      entityClassToProvide: Puesto,
      providerConfigClass: CRUDEntityProviderJSONFileConfig,
      providerClass: CRUDEntityProviderJSONFile,
      restServerUrl: sameAppServerUrl,
      apiBasePath: publicFolderWithJsonFiles,
      resourceName: "puestos.json",
      useLocalCachedDB: true
    },
    {
      entityClassNameToProvide: "Centro",
      entityClassToProvide: Centro,
      providerConfigClass: CRUDEntityProviderJSONFileConfig,
      providerClass: CRUDEntityProviderJSONFile,
      restServerUrl: sameAppServerUrl,
      apiBasePath: publicFolderWithJsonFiles,
      resourceName: "centros-trabajo.json",
      useLocalCachedDB: true
    },
    {
      entityClassNameToProvide: "Favorito",
      entityClassToProvide: Favorito,
      providerConfigClass: CRUDEntityProviderJSONFileConfig,
      providerClass: CRUDEntityProviderJSONFile,
      restServerUrl: sameAppServerUrl,
      apiBasePath: publicFolderWithJsonFiles,
      resourceName: "favoritos.json",
      useLocalCachedDB: true
    },
    {
      entityClassToProvide: Finalidad,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "finalidad",
      useLocalCachedDB: false
    },
    {
      entityClassToProvide: TipoFuente,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "tipo-fuente",
      useLocalCachedDB: false
    },
    {
      entityClassToProvide: Hito,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "hito",
      useLocalCachedDB: false
    },
    {
      entityClassToProvide: Dominio,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "dominio",
      useLocalCachedDB: false
    },
    {
      entityClassToProvide: PlanFinancion,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "plan-financiacion",
      useLocalCachedDB: false
    },
    {
      entityClassToProvide: Actuacion,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "actuacion",
      useLocalCachedDB: false
    },
    {
      entityClassToProvide: ProyectoFeder,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "proyecto-feder",
      useLocalCachedDB: false
    },
    {
      entityClassToProvide: ProgramaFinanciacion,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "programa-financiacion",
      useLocalCachedDB: false
    }/*,
    {
      entityClassToProvide: Finalidad,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "finalidad",
      useLocalCachedDB: false
    },
    {
      entityClassToProvide: TipoFuente,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "tipo-fuente",
      useLocalCachedDB: false
    },
    {
      entityClassToProvide: Hito,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "hito",
      useLocalCachedDB: false
    },
    {
      entityClassToProvide: Dominio,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "dominio",
      useLocalCachedDB: false
    },
    {
      entityClassToProvide: PlanFinancion,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "plan-financiacion",
      useLocalCachedDB: false
    },
    {
      entityClassToProvide: Actuacion,
      providerConfigClass: CRUDEntityProviderWSO2EIConfig,
      providerClass: CRUDEntityProviderWSO2EI,
      restServerUrl: esbURL,
      apiBasePath: esbApiName,
      resourceName: "actuacion",
      useLocalCachedDB: false
    }*/
  ]
};

export default appConfig;
