import {AuthenticationUser, AuthenticationUserPermissions, utils} from "src/vue-app-shared";

export async function actionGetAuthenticatedUserOrLogin(context) {
  let isUserLogged = await AuthenticationUser.provider.isLogged();

  if (!isUserLogged) {
    AuthenticationUser.provider.loginWithRedirect();
  } else {
    try {
      let authenticatedUser = await AuthenticationUser.provider.getAuthenticatedUser();

      if (_isTimeToReloadUserPermissions(context, authenticatedUser)) {
        /**
         * @type {AuthenticationUserPermissions}
         */
        let authenticatedUserPermissions;

        authenticatedUserPermissions = await AuthenticationUserPermissions.provider.readItem();
        context.commit("authenticateUserAndGetPermissions", { authenticatedUser, authenticatedUserPermissions });
      }
    } catch (error) {
      // todo: escribir error de autenticacion
    }
  }
}

export function authenticateUserAndGetPermissions(state, { authenticatedUser, authenticatedUserPermissions }) {
  state.authenticatedUser = authenticatedUser;
  state.authenticatedUserPermissions = authenticatedUserPermissions;
}

/**
 * Comprueba si no está cargado los permisos o si ha cambiado el objeto del usuario autenticado de alguna manera,
 * lo que indicaría que los permisos podŕian haber cambiado, luego hay que volver a cargarlos
 * @param {*} context
 * @param {*} authenticatedUser
 * @return {boolean}
 */
function _isTimeToReloadUserPermissions(context, authenticatedUser) {
  return !context.state.authenticatedUserPermissions || !utils.hasEqualsValues(authenticatedUser, context.state.authenticatedUser);
}
