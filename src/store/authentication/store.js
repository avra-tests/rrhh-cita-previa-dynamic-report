import { actionGetAuthenticatedUserOrLogin, authenticateUserAndGetPermissions} from "./actionLogin";

const actions = {
  actionGetAuthenticatedUserOrLogin
};

const mutations = {
  authenticateUserAndGetPermissions
};

const getters = null;

// Se devuelve como funcion para cuando se realicen varias instancias (p.e. en ssr) no se devuelva el mismo objeto
// En ssr cada navegador debe tener una instancia distinta y como lo ejecuta nodejs hay que hacerlo así
// (para cuando lo metamos)
const state = () => ({
  authenticatedUser: null,
  authenticatedUserPermissions: null
});

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
};
