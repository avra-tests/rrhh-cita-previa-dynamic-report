import { CRUDEntity, CRUDEntityProvider, utils } from "src/vue-app-shared";
import Cita from "./Cita";

class PlanificacionDiaBloque {
  /** @type {string} */ id;
  /** @type {string} */ title;
  /** Hora inicio @type {string} */ start; //	"08:00"
  /** Hora fin @type {string} */ end; //	"14:00"
}

class PlanificacionDia {
  /** @type {string} */ id;
  /** es día para poder pedir cita online  @type {boolean} */ available;
  /** @type {number} **/ weekdayId;
  /** es dia que se atienden cita @type {boolean} */ workingDay;
  /** @type {Array<PlanificacionDiaBloque>} */ capacityBlocks;
}

class PlanificacionSemanal {
  /** @type {string} */ id;
  /** @type {string} */ name;
  /** Hora inicio @type {string} */ start; //	"08:00"
  /** Hora fin @type {string} */ end; //	"14:00"
  /** @type {Array<PlanificacionDia>} */ capacityDays;
}

export default class PerfilCita extends CRUDEntity {
  /** @type {string} */ id;
  /** @type {string} */ nombre;
  /** @type {number} */ duracion;
  /** @type {number} */ diasReservables;
  /** @type {PlanificacionSemanal} */ planificacionSemanal;
  /** @type {object} */ oficina;
  /** @type {Array} */ servicios;
  /** @type {Array} */ recursos;

  constructor(fieldValuesObject) {
    // Si se hace un super no se actualizan los ids, después se llama a esta clase
    super();

    this._setEntityFieldsValues(fieldValuesObject);

    // this.readRelatedEntitiesProperties = async () => {
    // };
  }

  /**
   * @param {Date} date
   * @returns {boolean}
   */
  isDateInPlanificacion(date) {
    let weekDayForDate = utils.getWeekDayNumber(date);

    return this.planificacionSemanal.capacityDays[weekDayForDate].workingDay;
  }

  isTherePlanificacionBlocks() {
    return this.planificacionSemanal.capacityDays.some(
      planificacionDia => planificacionDia.capacityBlocks && planificacionDia.capacityBlocks.length > 0
    );
  }

  /**
   *
   * @param {Date} startDate
   * @param {object} currentOficina
   * @param {object} currentRecurso
   * @param {Array<Cita>} citas
   */
  createHuecosListForOficinaRecurso(startDate, currentOficina, currentRecurso, citas) {
    /** @type {Array<Cita>} */
    let citasHuecosList = [];
    let citasOficinaRecursoSortedByDate = Cita.sortArrayByFechaAsc(
      citas.filter(cita => cita.oficinaId === currentOficina.id).filter(cita => cita.recursoId === currentRecurso.id)
    );

    // if (!this.isTherePlanificacionBlocks()) {
    /** {Array<PlanificacionDia>} */
    let planificacionWeekDays = this.planificacionSemanal.capacityDays;
    let planificacionEndDate = utils.dateWithDaysAdded(startDate, 31);
    // let planificacionEndDate = utils.dateWithDaysAdded(new Date(), this.diasReservables);

    //let datesForPlanificacionPeriod = utils.createDaysBetweenDates(startDate, endDate);
    let datesForPlanificacionPeriod = utils
      .createDaysBetweenDates(startDate, planificacionEndDate)
      .filter(date => this.isDateInPlanificacion(date))
      .filter(date => this._isNotThereCitasForFullDay(date, citasOficinaRecursoSortedByDate));

    datesForPlanificacionPeriod.forEach(currentDate => {
      let huecosForDay = this._huecosListForDay(currentDate, currentOficina, currentRecurso, citasOficinaRecursoSortedByDate);

      citasHuecosList = citasHuecosList.concat(huecosForDay);
    });
    // }

    return citasHuecosList;
  }

  /**
   *
   * @param {Date} startDate
   * @param {object} oficina
   * @param {object} recurso
   * @param {Array<Cita>} citas
   */
  updateCitasWithHorarioLabel(startDate, oficina, recurso, citas) {
    citas
      .filter(cita => cita.oficinaId === oficina.id)
      .filter(cita => cita.recursoId === recurso.id)
      .forEach(cita => {
        cita.horario = this.getLabel();
      });
  }

  _isNotThereCitasForFullDay(day, citas) {
    let citasForFullDay = citas.filter(cita => {
      return this.getStartDateForDay(day) >= cita.fecha && this.getEndDateForDay(day) <= cita.fechaFin;
    });
    return citasForFullDay.length === 0;
  }

  /**
   *
   * @param {Cita} cita
   * @param {Date} day
   */
  _isCitaInDay(cita, day) {
    let citaStarDayString = utils.getYYMMDDDateString(cita.fecha);
    let citaEndDayString = utils.getYYMMDDDateString(cita.fechaFin);
    let dayString = utils.getYYMMDDDateString(day);
    let result;

    // if (citaStarDayString !== citaEndDayString) {
    //   result = citaStarDayString <= dayString && citaEndDayString >= dayString;
    // } else {
    result = citaStarDayString === dayString;
    return result;
  }

  _huecosListForDay(day, oficina, recurso, citasSortedByDate) {
    let citasForDay = citasSortedByDate.filter(cita => this._isCitaInDay(cita, day));

    let huecos = [];

    if (citasForDay.length === 0) {
      huecos.push(this._createHuecoFromPlanificacionDay(oficina, recurso, day));
    } else {
      citasForDay.forEach((currentCita, index, citasList) => {
        if (index === 0 && this._isTherePreviousFreeTimeInDayToCita(currentCita)) {
          huecos.push(this._createHuecoFromPlanificacionStartToCita(oficina, recurso, currentCita));
        } else if (index >= 1 && this._isThereFreeTimeBetweenCitas(citasList[index - 1], currentCita)) {
          huecos.push(this._createHuecoBetweenCitas(oficina, recurso, citasList[index - 1], currentCita));
        }

        // No se pone un else porque se puede dar el caso que una unica cita en el día cumpla el caso anterior y este
        if (index === citasList.length - 1 && this._isThereNextFreeTimeInDayToCita(currentCita)) {
          huecos.push(this._createHuecoFromCitaEndToPlanificacionEnd(oficina, recurso, currentCita));
        }
      });
    }

    return huecos;
  }

  getStartDateForDay(day) {
    return utils.getDateSettingHourMinutes(day, this.planificacionSemanal.start);
  }

  getEndDateForDay(day) {
    return utils.getDateSettingHourMinutes(day, this.planificacionSemanal.end);
  }

  getLabel() {
    let label = "";

    if (!this.isTherePlanificacionBlocks()) {
      label = `${this._getCapacityDaysLabel()}, ${this.planificacionSemanal.start} - ${this.planificacionSemanal.end}`;
    } else {
      label = `${this._getCapacityDaysLabel()}, ${this.planificacionSemanal.start} - ${this.planificacionSemanal.end}`;
    }

    return label;
  }

  _getCapacityDaysLabel() {
    let label = "";

    let weekdaysLetter = ["L", "M", "Mx", "J", "V", "S", "D"];

    this.planificacionSemanal.capacityDays.forEach((weekday, index) => {
      if (weekday.workingDay) {
        if (label.length > 0) {
          label = label.concat(",");
        }
        label = label.concat(weekdaysLetter[index] + "");
      }
    });

    return label;
  }

  _getCapacityBlocksLabel() {
    let label = "";

    let weekdaysLetter = ["L", "M", "Mx", "J", "V", "S", "D"];

    this.planificacionSemanal.capacityDays.forEach((weekday, index) => {
      if (weekday.workingDay) {
        let separator = label.length > 0 ? "," : "";
        label = `${label}${separator}${weekdaysLetter[index]}`;

        if (weekday.capacityBlocks && weekday.capacityBlocks.length > 0) {
          let blockLabel = "";
          weekday.capacityBlocks.forEach(planBlock => {
            let separator = blockLabel.length > 0 ? "," : "";
            blockLabel = `${blockLabel}${separator}${planBlock.start}-${planBlock.end}`;
          });

          label = `${label}(${blockLabel})`;
        }
      }
    });

    return label;
  }

  /**
   *
   * @param {object} oficina
   * @param {object} recurso
   * @param {Date} startDate
   * @param {Date} endDate
   */
  _createHueco(oficina, recurso, startDate, endDate) {
    return new Cita({
      id: null,
      titulo: null,
      fecha: startDate,
      fechaFin: endDate,
      nombre: null,
      apellidos: null,
      email: null,
      telefono: null,
      nif: null,
      oficina: oficina.name,
      oficinaId: oficina.id,
      servicio: null,
      recurso: recurso.name,
      recursoId: recurso.id,
      numeroCita: 1,
      duracion: utils.getMinutesBetweenDates(endDate, startDate),
      tipo: "Libre",
      horario: this.getLabel()
    });
  }

  _createHuecoFromPlanificacionDay(oficina, recurso, day) {
    return this._createHueco(oficina, recurso, this.getStartDateForDay(day), this.getEndDateForDay(day));
  }

  /**
   *
   * @param {object} oficina
   * @param {object} recurso
   * @param {Cita} cita
   */
  _createHuecoFromPlanificacionStartToCita(oficina, recurso, cita) {
    return this._createHueco(oficina, recurso, this.getStartDateForDay(cita.fecha), cita.fecha);
  }

  /**
   *
   * @param {object} oficina
   * @param {object} recurso
   * @param {Date} day
   * @param {Cita} cita
   */
  _createHuecoFromCitaEndToPlanificacionEnd(oficina, recurso, cita) {
    return this._createHueco(oficina, recurso, cita.fechaFin, this.getEndDateForDay(cita.fecha));
  }

  /**
   *
   * @param {Cita} previousCita
   * @param {Cita} cita
   */
  _createHuecoBetweenCitas(oficina, recurso, previousCita, cita) {
    return this._createHueco(oficina, recurso, previousCita.fechaFin, cita.fecha);
  }

  /**
   *
   * @param {Cita} cita
   */
  _isTherePreviousFreeTimeInDayToCita(cita) {
    return this.getStartDateForDay(cita.fecha) < cita.fecha;
  }

  /**
   * @param {Cita} cita
   */
  _isThereNextFreeTimeInDayToCita(cita) {
    return cita.fechaFin < this.getEndDateForDay(cita.fecha);
  }

  /**
   *
   * @param {Cita} previousCita
   * @param {Cita} cita
   */
  _isThereFreeTimeBetweenCitas(previousCita, cita) {
    return previousCita.fechaFin < cita.fecha;
  }
}

export { PerfilCita, PlanificacionDia, PlanificacionSemanal };
