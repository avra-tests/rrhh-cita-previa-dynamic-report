import { CRUDEntity } from "src/vue-app-shared";

export default class Centro extends CRUDEntity {
  /** @type {string} */ id;
  /** @type {object} */ filter;
  /** @type {object} */ reportDataTypes;
  /** @type {object} */ dataSource;
  /** @type {object} */ slice;
  /** @type {object} */ options;
  /** @type {object} */ localization;

  
  constructor(fieldValuesObject) {
    // Si se hace un super no se actualizan los ids, después se llama a esta clase
    super();
    this._setEntityFieldsValues(fieldValuesObject);
  }
}

  //   "dataSource": {
  //     "dataSourceType": "json",
  //     "data": []
  //   },
  //   "slice": {
  //     "rows": [
  //       {
  //         "uniqueName": "oficina",
  //         "sort": "unsorted"
  //       },
  //       {
  //         "uniqueName": "recurso",
  //         "sort": "unsorted"
  //       },
  //       {
  //         "uniqueName": "fecha.Year",
  //         "sort": "unsorted"
  //       },
  //       {
  //         "uniqueName": "fecha.Month", /** @type {string} */ id;
  //         /** @type {string} */ nombre;
  //         /** @type {string} */ direccion;
  //         /** @type {string} */ idProvincia;
  //         /** @type {string} */ idCentroTrabajoPadre;
        
  //           "dataSource": {
  //             "dataSourceType": "json",
  //             "data": []
  //           },
  //           "slice": {
  //             "rows": [
  //               {
  //                 "uniqueName": "oficina",
  //                 "sort": "unsorted"
  //               },
  //               {
  //                 "uniqueName": "recurso",
  //                 "sort": "unsorted"
  //               },
  //               {
  //                 "uniqueName": "fecha.Year",
  //                 "sort": "unsorted"
  //               },
  //         "sort": "unsorted"
  //       },
  //       {
  //         "uniqueName": "fecha.Day",
  //         "sort": "unsorted"
  //       },
  //       {
  //         "uniqueName": "hora",
  //         "sort": "unsorted"
  //       },
  //       {
  //         "uniqueName": "tipo",
  //         "sort": "unsorted"Informe
  //       },
  //       {
  //         "uniqueName": "duracion",
  //         "sort": "unsorted"
  //       },
  //       {
  //         "uniqueName": "cliente",
  //         "sort": "unsorted"
  //       },
  //       {
  //         "uniqueName": "telefono",
  //         "sort": "unsorted"
  //       },
  //       {
  //         "uniqueName": "email",
  //         "sort": "unsorted"
  //       },
  //       {
  //         "uniqueName": "horario",
  //         "sort": "unsorted"
  //       },
  //       {
  //         "uniqueName": "servicio",
  //         "sort": "unsorted"
  //       }
  //     ],
  //     "columns": [
  //       {
  //         "uniqueName": "Measures"
  //       }
  //     ],
  //     "measures": [
  //       {
  //         "uniqueName": "duracion",
  //         "aggregation": "sum"
  //       }
  //     ]
  //   },
  //   "options": {
  //     "grid": {
  //       "type": "flat",
  //       "title": "INFORME DE DETALLE DE CITAS, HUECOS Y HORARIOS CERRADOS",
  //       "showTotals": "off",
  //       "showGrandTotals": "off"
  //     }
  //   },
  //   "localization": "https://cdn.webdatarocks.com/loc/es.json"
  // }