import { CRUDEntity, CRUDEntityProvider, utils } from "src/vue-app-shared";
import PerfilCita from "./PerfilCita";

export default class Cita extends CRUDEntity {
  /** @type {string} */ id;
  /** @type {string} */ titulo;
  /** @type {Date} */ fecha;
  /** @type {Date} */ fechaFin;
  /** @type {string} */ nombre;
  /** @type {string} */ apellidos;
  /** @type {string} */ email;
  /** @type {string} */ telefono;
  /** @type {string} */ nif;
  /** @type {string} */ oficina;
  /** @type {string} */ oficinaId;
  /** @type {string} */ servicio;
  /** @type {string} */ servicioId;
  /** @type {string} */ recurso;
  /** @type {string} */ recursoId;
  /** @type {number} */ numeroCitas;
  /** @type {string} */ tipo;
  /** @type {string} */ hora;
  /** @type {string} */ horario;
  /** @type {number} */ duracion;
  /** @type {string} */ observaciones;
  

  constructor(fieldValuesObject) {
    // Si se hace un super no se actualizan los ids, después se llama a esta clase
    super();

    this._setEntityFieldsValues(fieldValuesObject);

    this.id = fieldValuesObject.id ?? this._generateNewId();
    this.hora = this._getHora();

    this.readRelatedEntitiesProperties = async () => {};
  }

  _generateNewId() {
    return `GENERATED${this.oficinaId}${this.recursoId}${this.fecha.toISOString()}`;
  }

  /**
   * @param {Array<Cita>} citas
   * @returns {Array<Cita>}
   */
  static sortArrayByFechaAsc(citas) {
    return citas.sort((cita1, cita2) => cita1.fecha - cita2.fecha);
  }

  _getHora() {
    let horaInicio = this._getHourMinuteString(this.fecha);
    let horaFin = this._getHourMinuteString(this.fechaFin);
    let fechaInicio = this.fecha.toLocaleDateString("es-ES");
    let fechaFin = this.fechaFin.toLocaleDateString("es-ES");

    if (fechaInicio !== fechaFin) {
      horaInicio =  `${horaInicio} (${fechaInicio})`;
      horaFin =  ` ${horaFin} (${fechaFin})`;
    }

    return `${horaInicio} - ${horaFin}`
  }
  _getHourMinuteString(date) {
    return utils.getHHMMTimeString(date);
  }
}

