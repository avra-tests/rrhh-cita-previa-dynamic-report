import {
  StoreGenericUseCaseListHelper,
  AppError,
  EntityList,
  utils
} from "src/vue-app-shared";
import Cita from "./Cita";
import Informe from "./Informe";
import PerfilCita from "src/store/citas/PerfilCita";

// let storeCaseListHelper = new StoreGenericUseCaseListHelper({
//   useCaseName: "viewCitasList",
//   stateLoadingParamName: "isCitasListLoading",
//   stateListParamName: "citasList"
// });

// export default {
//   actions: {
//     async actionViewCitasList(context, payload) {
//       // Se tienen que cargar primero los perfiles de cita, si lo sé, una chapú como la casa godzilla...
//       await PerfilCita.provider.readList(payload.listParams);
//       const informesList = await Informe.provider.readList(payload.listParams);

//       console.log("Informes", informesList.items[0]);

//       const entityReadListFunction = () => Cita.provider.readList(payload.listParams);
//       storeCaseListHelper.actionViewEntityList({ context, payload, listParamsName: "listParams", entityReadListFunction });

//     }
//   },
//   mutations: storeCaseListHelper.getMutations() // { <casename>Begin, <casename>End, <casename>EndwithError }
// };

export default {
  actions: {
    async actionViewInformeCitasPorDiasList(context, payload) {
      try {
        if (
          !payload &&
          !payload.listParams &&
          !payload.listParams &&
          !payload.isListShowedAsGrid
        ) {
          throw new AppError(
            "actionViewCitasPorDiaList: Se esperaba listParam y no se ha recibido nada"
          );
        }

        let listParams = payload.listParams;

        context.commit("mutationViewInformeCitasPorDiasListBegin");

        await PerfilCita.provider.readList(payload.listParams);
        const informesList = await Informe.provider.readList(
          payload.listParams
        );
        const informe = utils.cloneObjectValues((informesList.items[1]));

        // console.log("informesList.items", informesList.items);

        // const informe = informesList.items[0];

        console.log("informe.filter.oficina", informe.filter, informe.options.grid);

        /** @type {EntityList} */
        const citasList = await Cita.provider.readList(payload.listParams);

        const itemsFilteredByInforme = citasList.items.filter(
          (cita) => 
          (cita.oficina === informe.filter.oficina) && (cita.tipo === "Reserva")
        );

        // TODO: cutre hacer que se lea bien el rest
        const citasListByInformeParams = new EntityList({
          items: itemsFilteredByInforme,
          itemsTotalCount: itemsFilteredByInforme.length,
          params: citasList.params,
        });

        context.commit("mutationViewInformeCitasPorDiasListEnd", {
          informesList,
          citasList: citasListByInformeParams,
        });
      } catch (error) {
        AppError.handleError(error);
        context.commit("mutationViewInformeCitasPorDiasListEndWithError");
        throw error;
      }
    },
  },
  mutations: {
    mutationViewInformeCitasPorDiasListBegin(state) {
      state.isInformeCitasListLoading = true;
    },

    mutationViewInformeCitasPorDiasListEndWithError(state) {
      state.isInformeCitasListLoading = false;
    },

    mutationViewInformeCitasPorDiasListEnd(state, { informesList, citasList }) {
      state.informesList = informesList;
      state.citasList = citasList;
      state.isInformeCitasListLoading = false;
    },
  },
};
