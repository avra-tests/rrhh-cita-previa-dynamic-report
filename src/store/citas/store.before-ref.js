import {
  actionViewEmpleadosList,
  mutationViewEmpleadosListBegin,
  mutationViewEmpleadosListEnd,
  mutationViewEmpleadosListEndWithError
} from "./actionViewEmpleadosList";

import {
  actionViewEmpleadosItem,
  mutationViewEmpleadoItemBegin,
  mutationViewEmpleadoItemEnd,
  mutationViewEmpleadoItemEndWithError
} from "./actionViewEmpleadosItem";

import {
  actionViewFavoritosList,
  mutationViewFavoritosListBegin,
  mutationViewFavoritosListEnd,
  mutationViewFavoritosListEndWithError
} from "./actionViewFavoritosList";

const actions = {
  actionViewEmpleadosList,
  actionViewEmpleadosItem,
  actionViewFavoritosList
};

const mutations = {
  mutationViewEmpleadosListBegin,
  mutationViewEmpleadosListEnd,
  mutationViewEmpleadosListEndWithError,
  mutationViewEmpleadoItemBegin,
  mutationViewEmpleadoItemEnd,
  mutationViewEmpleadoItemEndWithError,
  mutationViewFavoritosListBegin,
  mutationViewFavoritosListEnd,
  mutationViewFavoritosListEndWithError
};

const getters = null;

// Se devuelve como funcion para cuando se realicen varias instancias (p.e. en ssr) no se devuelva el mismo objeto
// En ssr cada navegador debe tener una instancia distinta y como lo ejecuta nodejs hay que hacerlo así
// (para cuando lo metamos)
const state = () => ({
  empleadosList: null,
  isEmpleadosListLoading: false,
  empleado: null,
  isEmpleadoLoading: false,
  favoritosList: null,
  isFavoritosListLoading: false
});

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
};
