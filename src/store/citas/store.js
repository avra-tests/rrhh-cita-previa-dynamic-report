import caseViewInformeCitasList from "./caseViewInformeCitasList";
import caseViewInformeCitasPorDiaList from "./caseViewInformeCitasPorDiaList";

// Se devuelve como funcion para cuando se realicen varias instancias (p.e. en ssr) no se devuelva el mismo objeto
// En ssr cada navegador debe tener una instancia distinta y como lo ejecuta nodejs hay que hacerlo así
// (para cuando lo metamos)
const state = () => ({
  citasList: null,
  informesList: null,
  isInformeCitasListLoading: false,
});

export default {
  namespaced: true,
  getters: {},
  mutations: {
    ...caseViewInformeCitasList.mutations,
    ...caseViewInformeCitasPorDiaList.mutations
  },
  actions: {
    ...caseViewInformeCitasList.actions,
    ...caseViewInformeCitasPorDiaList.actions
  },
  state
};
