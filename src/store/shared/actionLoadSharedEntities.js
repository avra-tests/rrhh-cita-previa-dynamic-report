import { AppError, utils } from "src/vue-app-shared";


/**
 * Acción para cargar las entidades que se comparten en toda la aplicación
 *
 * @param {*} context
 */
export async function actionLoadSharedEntities(context) {
  try {
    // context.commit("mutationLoadSharedEntitiesBegin");
    // let provinciasList = await Provincia.provider.readList();
    // let provinciasById = provinciasList.convertToMapById("id");
    // let provinciasSelectOptions = provinciasList.convertToSelectOptions("nombre", "id");

    // let puestosList = await Puesto.provider.readList();
    // let puestosById = puestosList.convertToMapById("id");
    // let puestosSelectOptions = puestosList.convertToSelectOptions("nombre", "id");

    // let centrosList = await Centro.provider.readList();
    // let centrosById = centrosList.convertToMapById("id");
    // let centrosSelectOptions = centrosList.convertToSelectOptions("nombre", "id");

    // let dominioList = await Dominio.provider.readList();
    // let dominioById = dominioList.convertToMapById("id");
    // let dominioSelectOptions = dominioList.convertToSelectOptionsWithDomain("descripcion", "id", "dominio");

    // let hitoList = await Hito.provider.readList();
    // let hitoById = hitoList.convertToMapById("idHito");
    // let hitoSelectOptions = hitoList.convertToSelectOptions("nombre", "idHito");

    // let tipoFuenteList = await TipoFuente.provider.readList();
    // let tipoFuenteById = tipoFuenteList.convertToMapById("idTipoFuente");
    // let tipoFuenteSelectOptions = tipoFuenteList.convertToSelectOptions("nombre", "idHito");

    context.commit("mutationLoadSharedEntitiesEnd", {
      provinciasById,
      provinciasSelectOptions,
      puestosById,
      puestosSelectOptions,
      centrosById,
      centrosSelectOptions
      // dominioList,
      // dominioById,
      // dominioSelectOptions,
      // hitoById,
      // hitoSelectOptions,
      // tipoFuenteById,
      // tipoFuenteSelectOptions
    });
  } catch (error) {
    AppError.handleError(error);
    context.commit("mutationLoadSharedEntitiesEndWithError");
    throw error;
  }
}

export function mutationLoadSharedEntitiesBegin(state) {
  state.isSharedEntitiesLoaded = false;
}

export function mutationLoadSharedEntitiesEnd(
  state,
  {
    provinciasById,
    provinciasSelectOptions,
    puestosById,
    puestosSelectOptions,
    centrosById,
    centrosSelectOptions
    // dominioById,
    // dominioSelectOptions,
    // hitoById,
    // hitoSelectOptions,
    // tipoFuenteById,
    // tipoFuenteSelectOptions
  }
) {
  state.provinciasById = provinciasById;
  state.provinciasSelectOptions = provinciasSelectOptions;
  state.puestosById = puestosById;
  state.puestosSelectOptions = puestosSelectOptions;
  state.centrosById = centrosById;
  state.centrosSelectOptions = centrosSelectOptions;
  // state.dominioById = dominioById;
  // state.dominioSelectOptions = dominioSelectOptions;
  // state.hitoById = hitoById;
  // state.hitoSelectOptions = hitoSelectOptions;
  // state.tipoFuenteById = tipoFuenteById;
  // state.tipoFuenteSelectOptions = tipoFuenteSelectOptions;
  state.isSharedEntitiesLoaded = true;
}

export function mutationLoadSharedEntitiesEndWithError(state) {
  state.isSharedEntitiesLoaded = false;
}
