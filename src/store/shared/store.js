import {
  actionLoadSharedEntities,
  mutationLoadSharedEntitiesBegin,
  mutationLoadSharedEntitiesEnd,
  mutationLoadSharedEntitiesEndWithError
} from "./actionLoadSharedEntities";

const actions = {
  actionLoadSharedEntities
};

const mutations = {
  mutationLoadSharedEntitiesBegin,
  mutationLoadSharedEntitiesEnd,
  mutationLoadSharedEntitiesEndWithError
};

const getters = null;

// Se devuelve como funcion para cuando se realicen varias instancias (p.e. en ssr) no se devuelva el mismo objeto
// En ssr cada navegador debe tener una instancia distinta y como lo ejecuta nodejs hay que hacerlo así
// (para cuando lo metamos)
const state = () => ({
  provinciasById: {},
  provinciasSelectOptions: [],
  puestosById: {},
  puestosSelectOptions: [],
  centrosById: {},
  centrosSelectOptions: [],
  dominioById:{},
  dominioSelectOptions: [],
  hitoById:{},
  hitoSelectOptions: [],
  tipoFuenteById:{},
  tipoFuenteSelectOptions: [],
  isSharedEntitiesLoaded: false
});

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
};
